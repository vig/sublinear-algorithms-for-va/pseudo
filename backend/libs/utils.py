"""
Copyright 2018 Novartis Institutes for BioMedical Research Inc.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import numpy as np
from sklearn.preprocessing import MinMaxScaler



def normalize(data, percentile: float = 99.9):
    cutoff = np.percentile(data, (0, percentile))
    data_norm = np.copy(data)
    data_norm[np.where(data_norm < cutoff[0])] = cutoff[0]
    data_norm[np.where(data_norm > cutoff[1])] = cutoff[1]

    return MinMaxScaler().fit_transform(data_norm)
