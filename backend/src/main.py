import logging
from pathlib import Path
from time import time, perf_counter
from typing import Any, Dict, List, Union

import numpy as np
import orjson
import yaml
from flask import Flask, request
from flask_cors import CORS

from src import lsh
from src.preprocessing import Preprocessor

logging.basicConfig(level=logging.INFO)
path_global_config_yml = Path("..") / "config.yml"


# Read config
def load_config(path_global_config_yml: Union[Path, str] = path_global_config_yml) -> Dict[str, Any]:
    """Read the global configuration file

    Parameters
    ----------
    path_global_config_yml : Path | str, default Path("../config.yml")
        Path of the global configuration YAML-file.

    Returns
    -------
    Dict[str, Any]
        Global configuration, including which dataset to use.

    """
    with open(path_global_config_yml, "r") as fp:
        config = yaml.load(fp, yaml.FullLoader)
    # reload = False
    reload = config["reload"]
    path_config_yml = Path(config["path_config_yml"])
    with open(path_config_yml, "r") as fp:
        config = yaml.load(fp, yaml.FullLoader)

    return config


config = load_config()
preprossessor = Preprocessor(
    dataset=config["dataset"],
    path_data_hdf=Path(config["path_data_hdf"]),
    path_meta_json=Path(config["path_meta_json"]),
    channel_names=config["channels"],
    dir_in_hdf=config["dir_in_hdf"],
    dir_cache=Path(config["dir_cache"]),
)

app = Flask(__name__)
CORS(app)


@app.route("/", methods=["GET"])
def index():
    return "hi"


@app.route("/load-data", methods=["GET"])
def load_data():
    """Load raw data.

    Returns
    -------
    bytes
        Loaded data with the following interface
            {
                index: 1d array [x],
                values: 1d array [x],
                name: str
            }[]

    """

    logging.info("Loading data ...")
    time_start = perf_counter()

    response = preprossessor.load_data()
    response = orjson.dumps(response)

    logging.info(f"Completed loading data in {perf_counter() - time_start:.2f} second(s).")

    return response


@app.route("/create-windows", methods=["POST"])
def create_windows():
    """Creates windows to transform the local pattern search problem to time series indexing.

    Input from request with interface
        {
            parameters: {
                windowssize: int
            }
        }

    Returns
    -------
    '1'

    """

    logging.info("Creating window ...")
    time_start = perf_counter()

    if not preprossessor.path_preprocessed_data_npy_.is_file():
        raw_data = request.json
        window_size = int(raw_data["parameters"]["windowsize"])
        preprossessor.create_windows(window_size)

    logging.info(f"Completed windows in {perf_counter() - time_start:.2f} second(s).")

    return "1"


@app.route("/initialize", methods=["POST"])
def initialize():
    """Conduct the initial LSH.

    Input from request with interface
        {
            query: 2d array [d][t]
        }

    Returns
    -------
    bytes
        Response with interface
            {
                hash_functions: 3d array [k][l][d],
                candidates: 3d array [k][l][i],
                distances: 3d array [k][l][i],
                average_candidates: 1d array [i],
                average_distances: 1d array [i],
                tables: {
                    bucket: 1d array
                }[],
                average_table: {
                    bucket: 1d array
                },
                samples: 1d array,
                parameters: 1d array
            }

    """

    logging.info("Starting the initial LSH ...")
    time_start = perf_counter()

    # Read windows
    raw_data = orjson.loads(request.data)
    data = np.load(preprossessor.path_preprocessed_data_npy_)
    data = np.swapaxes(data, 1, 2)  # Use a column vector for each channel

    # Read the query
    query = raw_data["query"]
    query = np.swapaxes(query, 0, 1)
    # parameters = np.load('parameters.npy')

    # Run the initial LSH
    logging.info("Executing the initial LSH ...")
    lsh_data = lsh.lsh(data, query)
    response = orjson.dumps(lsh_data)
    logging.info(f"Completed the initial LSH in {perf_counter() - time_start:2f} second(s)")

    return response


@app.route("/get-lsh-parameters", methods=["POST"])
def get_lsh_parameters():
    """Calculates LSH parameters based on the dataset

    Input window size "windowsize" of int from request.

    Returns
    -------
    bytes
        Optimal LSH parameters r, a, sd in shape Tuple[float, float, float].

    """
    logging.info("Calculating LSH parameters ...")
    time_start = perf_counter()
    raw_data = orjson.loads(request.data)
    window_size = raw_data["windowsize"]
    data = np.load(preprossessor.path_preprocessed_data_npy_)
    data = np.swapaxes(data, 1, 2)
    lsh_parameters = lsh.get_lsh_parameters(data, window_size)
    response = orjson.dumps(lsh_parameters)
    logging.info(f"Completed calculating LSH parameters in {perf_counter() - time_start:2f} second(s)")

    return response


@app.route("/update", methods=["POST"])
def update():
    """Does LSH and returns a bunch of useful information

    Input from request {
        query: 2d array [d][t],
        weights: 1d array [d],
        parameters: Tuple[float, float, float].
    }

    Returns
    -------
    bytes
        LSH data with interface {
            hash_functions: 3d array [k][l][d],
            candidates: 3d array [k][l][i],
            distances: 3d array [k][l][i],
            average_candidates: 1d array [i],
            average_distances: 1d array [i],
            tables: [{
                bucket: 1d array
            }],
            average_table: {
                bucket: 1d array
            },
            samples: 1d array
        }

    """
    logging.info("Performing LSH ...")
    time_start = perf_counter()
    raw_data = orjson.loads(request.data)
    data = np.load(preprossessor.path_preprocessed_data_npy_)
    data = np.swapaxes(data, 1, 2)
    query = raw_data["query"]
    query = np.swapaxes(query, 0, 1)
    weights = raw_data["weights"]
    parameters = raw_data["parameters"]

    lsh_data = lsh.lsh(data, query, parameters=parameters, weights=weights)
    response = orjson.dumps(lsh_data)
    logging.info(f"Completed LSH in {perf_counter() - time_start:2f} second(s)")

    return response


@app.route("/weights", methods=["POST"])
def weights():
    """Update channel weights according to the relevance-feedback

    Input from request with interface
        {
            labels: 1d array [?],
            hash_functions: 2d array [?][d],
            query: 2d array [d][t],
            weights: 1d array [d]
        }

    Returns
    -------
    bytes
        New weights in shape 1d array [d]

    """
    logging.info("Calculating new weights in the LSH model...")
    raw_data = orjson.loads(request.data)
    labels = raw_data["labels"]
    hash_functions = raw_data["hash_functions"]
    query = raw_data["query"]
    old_weights = raw_data["weights"]
    data = np.load(preprossessor.path_preprocessed_data_npy_)

    new_weights = lsh.weights(data, query, old_weights, labels, hash_functions)
    response = orjson.dumps(new_weights)

    return response


@app.route("/query", methods=["POST"])
def query():
    """Extract the initial query or calculate the query with DBA based on the given indices.

    Input from request with interface
        {
            start_index: int,
            query_size: int,
            indices: 1d array [?]
        }

    Returns
    -------
    bytes
        Updated query in shape 2d array [d][t].

    """
    logging.info("Preparing the query ...")
    time_start = perf_counter()

    raw_data = orjson.loads(request.data)
    start_index = raw_data["start_index"]
    query_size = raw_data["query_size"]
    window_indices = raw_data["indices"]

    if start_index is not None:
        preprossessor.create_windows(query_size)
        window_indices = int(start_index)

    data_preprocessed = np.load(preprossessor.path_preprocessed_data_npy_)
    response = lsh.query(data_preprocessed, window_indices)
    response = orjson.dumps(response)
    logging.info(f"Completed preparing the query in {time() - time_start}.")

    return response


@app.route("/window", methods=["POST"])
def window():
    """Returns values of windows on given indices.

    Input from request with interface
        {
            indices: 1d array [x]
        }

    Returns
    -------
    bytes
        Selected windows in shape 3d array [x][d][t].

    """
    logging.info("Extract windows ...")
    time_start = perf_counter()
    raw_data = orjson.loads(request.data)
    indices = raw_data["indices"]

    output = np.load(preprossessor.path_preprocessed_data_npy_)[indices]

    response = orjson.dumps(output.tolist())
    logging.info(f"Completed extracting windows in {time() - time_start}.")
    return response


@app.route("/table-info", methods=["POST"])
def table_info():
    """Returns additional information on given hash table.

    Input from request with interface
        {
            table: 2d array [x][?]
        }

    Returns
    -------
    bytes
        {
            prototypes: {
                average: 1d array [t]
                max: 1d array [t]
                min: 1d array [t]
            }
            distances: 2d array [x][x]
        }

    """
    time_start = perf_counter()
    raw_data = orjson.loads(request.data)
    table = raw_data["table"]
    data = np.load(preprossessor.path_preprocessed_data_npy_)

    response = lsh.table_info(data, table)
    logging.info(f"Averages calculated in {time() - time_start}.")

    return response
