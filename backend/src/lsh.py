import logging
import math
import random
from collections import defaultdict
from pathlib import Path
from time import time, perf_counter
from typing import Any, Dict, List, Optional, Sequence, Tuple, Union

import _lsh
import _ucrdtw
import numpy as np
from tslearn.barycenters import softdtw_barycenter
from tslearn.metrics import dtw


def get_lsh_parameters(data: np.ndarray, window_size: int) -> Tuple[float, float, float]:
    """Get optimal LSH parameters

    If the optimal LSH parameters are already calculated, load it from the cache, otherwise calculate them and save them as cache.

    Parameters
    ----------
    data : np.ndarray
        Preprocessed data, 3d array [m][t][d].
    window_size : int
        Window length

    Returns
    -------
    Tuple[float, float, float]
        Optimal LSH parameters r, a and sd.

    """
    path_parameters_npy = Path(f"cache/parameters_{window_size}.npy")
    if not path_parameters_npy.is_file():
        parameters = _calculate_lsh_parameters(data)
        np.save(str(path_parameters_npy), list(map(float, parameters)))
    return np.load(str(path_parameters_npy)).tolist()  # r, a, sd


def lsh(
    data: np.ndarray, query: np.ndarray, parameters: Optional[Sequence[float]] = None, weights: Optional[Sequence[float]] = None
) -> Dict[str, Any]:
    """
    Run locality-sensitive hashing.

    Parameters
    ----------
    data : np.ndarray
        Preprocessed data with the shape 3d array [m][t][d].
    query : Optional[np.ndarray]
        Query with the shape 2d array [t][d].
    parameters : Optional[Sequence[float]]
        Optimal LSH parameters r, a and sd.
    weights : Optional[Sequence[float]]
        Channel weights, 1d array [d].

    Returns
    -------
    Dict[str, Any]
        response.

    """
    if parameters is None:
        parameters = _calculate_lsh_parameters(data)
    r = parameters[0]
    a = parameters[1]
    sd = parameters[2]

    data = np.array(data, dtype="float32")
    query = np.array(query, dtype="float32")

    if weights is None:
        candidates, distances, hf = _lsh.lsh(data, query, r, a, sd, 1)
    else:
        candidates, distances, hf = _lsh.lsh(data, query, r, a, sd, 1, weights)

    dict_ = defaultdict(int)
    for l in range(len(candidates)):
        for k in range(len(candidates[0])):
            for i in range(len(candidates[0][0])):
                dict_[candidates[l][k][i]] += distances[l][k][i]
    sorted_dict = {k: v for k, v in sorted(dict_.items(), key=lambda item: item[1])}
    average_candidates = np.array(list(sorted_dict.keys())).tolist()
    average_distances = np.array(list(sorted_dict.values())).tolist()

    tables = []
    samples_set = set()
    candidates = candidates.tolist()
    for l in range(len(candidates)):
        for k in range(len(candidates[0])):
            samples_set.update(candidates[l][k][0:5])
            samples_set.update(random.sample(candidates[l][k], 5))
            dict_ = defaultdict(list)
            length = len(distances[l][k])
            median = distances[l][k][math.ceil(length / 2)]
            stepsize = median / 10
            indices = list(map(lambda x: 19 if x > median * 2 else math.floor(x / stepsize), distances[l][k]))
            for i in range(len(candidates[0][0])):
                dict_[str(indices[i])].append(candidates[l][k][i])
            tables.append(dict_)

    length = len(average_distances)
    median = average_distances[math.ceil(length / 2)]
    stepsize = median / 10
    indices = list(map(lambda x: 19 if x > median * 2 else math.floor(x / stepsize), average_distances))
    average_table = defaultdict(list)
    for i in range(len(average_candidates)):
        average_table[str(indices[i])].append(average_candidates[i])

    data = np.swapaxes(data, 1, 2)
    windows = data[average_candidates[:100]]
    average_values = np.average(windows, 0)
    std_values = np.std(windows, 0)
    max_values = average_values + std_values
    min_values = average_values - std_values
    prototype = {"average": average_values.tolist(), "max": max_values.tolist(), "min": min_values.tolist()}

    samples = np.array(list(filter(lambda x: x in samples_set, average_candidates))).tolist()

    response = {
        "hash_functions": hf.reshape((len(candidates) * len(candidates[0]), len(query[0]))).tolist(),
        "candidates": candidates,
        "distances": distances.tolist(),
        "average_candidates": average_candidates,
        "average_distances": average_distances,
        "tables": tables,
        "average_table": average_table,
        "samples": samples,
        "prototype": prototype,
        "parameters": [float(r), float(a), float(sd)],
    }
    return response


def _calculate_lsh_parameters(data: np.ndarray, r: Optional[float] = None) -> Tuple[float, float, float]:
    """Calculate optimal LSH parameters.

    Parameters
    ----------
    data : np.ndarray
        Preprocessed data with the shape 3d array [m][t][d].
    r : Optional[float]
        LSH parameter r.

    Returns
    -------
    Tuple[float]
        Optimal LSH parameters r, a and sd.

    """
    subset = []
    time_start = perf_counter()
    if r is None:
        r = data.shape[2]
    started = False
    first = 0
    last = r * 2
    i = 0
    print("r = " + str(r))

    # Use binary search to find optimal parameters (added simple constraints to speed up)
    while True:
        state = 1
        for s in subset:
            if np.linalg.norm(data[i] - data[s]) < r:
                state = 0
                break
        if state == 1:
            subset.append(i)

        i = i + 1
        if len(subset) > 400:
            print("bigger")
            if not started:
                first = r
                last = last * 2
            else:
                first = r
            r = (first + last) / 2
            subset = []
            i = 0
            print("r = " + str(r))
        elif (i == 10000 or i == len(data)) and len(subset) < 10:
            print("smaller")
            started = True
            last = r
            r = (first + last) / 2  # r / 2
            subset = []
            i = 0
            print("r = " + str(r))
        elif i == len(data):
            break

    dtw_distances = []
    eq_distances = []
    for i, index_1 in enumerate(subset):
        for j, index_2 in enumerate(subset):
            if index_1 == index_2:
                continue
            e = np.linalg.norm(data[index_1] - data[index_2])
            if math.isnan(e) or e == 0:
                eq_distances.append(0.0001)
                dtw_distances.append(0.0001)
                continue
            eq_distances.append(e)
            d = dtw(data[index_1], data[index_2], global_constraint="sakoe_chiba", sakoe_chiba_radius=int(0.05 * 120))
            dtw_distances.append(d)

    ratios = np.array(dtw_distances) / np.array(eq_distances)
    mean_dtw = np.mean(dtw_distances)
    sd_dtw = np.std(dtw_distances)
    mean_eq = np.mean(eq_distances)
    sd_eq = np.std(eq_distances)
    a = float(np.mean(ratios))
    sd = float(np.std(ratios))
    theta = float(mean_dtw + -2.58 * sd_dtw)
    # theta = mean_eq + -2.58 * sd_eq
    r = float(theta / ((a - sd) * math.sqrt(120)))
    if r < 0:
        logging.info(f"Actual r: {r}")
        r = mean_dtw / 100
    # r = theta / (math.sqrt(120))
    logging.info(f"Mean: {mean_dtw}")
    logging.info(f"Std: {sd_dtw}")
    logging.info(f"Ratio mean: {a}")
    logging.info(f"Ratio std: {sd}")
    logging.info(f"Theta: {theta}")
    logging.info(f"r: {r}")
    logging.info(f"Preprocessing time: {perf_counter()-time_start}")

    return r, a, sd


def weights(
    data: np.ndarray, query: np.ndarray, old_weights: Sequence[float], labels: np.ndarray, hash_functions: np.ndarray
) -> List[float]:
    """Update channel weights.

    Parameters
    ----------
    data : np.ndarray
        Preprocessed data in shape 3d array [m][d][t].
    query : Optional[np.ndarray]
        Query in shape 2d array [t][d].
    old_weights : List[float]
        Old channel weights in shape 1d array [d].
    labels : np.ndarray
        True positive labels in shape 1d array [?].
    hash_functions : np.ndarray
        Correct hash functions in shape 2d array [L*K][d]

    Returns
    -------
    List[float]
        Updated channel weights in shape 1d array [d].

    """

    def normalize(array: np.ndarray) -> np.ndarray:
        """Normalize a vector with its first norm."""
        array /= np.sum(array)
        array *= d
        return np.sqrt(array)

    def normalize_hash_functions(hfs: np.ndarray) -> np.ndarray:
        """Normalize hash functions"""
        return normalize(np.square(np.sum(hash_functions, axis=0)))

    alpha = 0.2
    d = len(query)
    logging.info(f"Dimensions: {d}")

    all_good_windows = data[[[int(index) for index, value in labels.items() if value is True]]]

    good_distances = np.zeros(len(query))
    for window in all_good_windows:
        for i in range(len(all_good_windows[0])):
            good_distances[i] += _ucrdtw.ucrdtw(query[i], window[i], 0.05, False)[1]
    if len(all_good_windows) != 0:
        good_distances = np.square(good_distances)
        if np.sum(good_distances) != 0:
            good_distances /= np.sum(good_distances)
        good_distances = np.ones(len(query)) - good_distances
        good_distances = normalize(good_distances)

    # if len(hash_functions) != 0:
    #     summed_hash_functions = np.sum(hash_functions, axis=0)
    #     summed_hash_functions = np.square(summed_hash_functions)
    #     normalized_hash_functions = normalize(summed_hash_functions)

    if len(hash_functions) + len(all_good_windows) == 0:
        logging.info(f"No update required.")
        new_weights = old_weights
    elif len(hash_functions) == 0:
        logging.info("Only update with the true positive labels.")
        new_weights = alpha * np.array(old_weights) + (1 - alpha) * good_distances
        new_weights = normalize(np.square(new_weights))
    elif len(all_good_windows) == 0:
        logging.info("Only update with the correct hash tables.")
        new_weights = alpha * np.array(old_weights) + (1 - alpha) * normalize_hash_functions(hash_functions)
        new_weights = normalize(np.square(new_weights))
    else:
        logging.info("Update with both the true positive labels and the correct hash tables.")
        new_weights = (
            alpha * np.array(old_weights)
            + 0.5 * (1 - alpha) * good_distances
            + 0.5 * (1 - alpha) * normalize_hash_functions(hash_functions)
        )
        new_weights = normalize(np.square(new_weights))

    logging.info(f"New weights: {new_weights}")
    # print(new_weights)

    return new_weights.tolist()


def table_info(data: np.ndarray, table: Sequence[Sequence[int]]) -> Dict[str, Any]:
    """Create prototypes.

    Parameters
    ----------
    data : np.ndarray
        Preprocessed data in shape 3d array [m][d][t].
    table : Sequence[Sequence[int]]
        Hash tables in shape 2d array [x][?]

    Returns
    -------
    Dict[str, Any]
        Prototypes and distances.

    """
    prototypes = []
    for cluster in table:
        windows = data[cluster]
        average_values = np.average(windows, 0)
        std_values = np.std(windows, 0)
        max_values = average_values + std_values
        min_values = average_values - std_values
        prototypes.append({"average": average_values.tolist(), "max": max_values.tolist(), "min": min_values.tolist()})
    return {"prototypes": prototypes, "distances": []}


def query(data: np.ndarray, window_indices: Union[int, Dict[int, bool]]) -> List[List[float]]:
    """Prepare the query.

    Define the query as the chosen window or update the query using DBA with true positive labels.

    Parameters
    ----------
    data : np.ndarray
        Preprocessed data in shape 3d array [m][d][t].
    window_indices : Union[int, Dict[int, bool]]
        Indices of true positive labels.

    Returns
    -------
    List[List[float]]
        Updated query.

    """
    if isinstance(window_indices, int):
        output = data[window_indices]
        print(output.tolist())
    else:
        indices = [int(index) for index, value in window_indices.items() if value is True]
        indices_windows = data[indices]
        output = softdtw_barycenter(np.swapaxes(indices_windows, axis1=1, axis2=2)).T
    return output.tolist()


def debug_test_lsh():
    data = np.load("cache/preprocessed_data.npy")
    # data = np.repeat(data, repeats=7, axis=1)
    print(data.shape)
    data = np.reshape(data, (len(data), len(data[0][0]), len(data[0])))
    data = np.array(data, dtype="float32")
    print(data.shape)

    r, a, sd = _calculate_lsh_parameters(data, 11.25)
    query_n = 1234
    t0 = time()
    query = data[query_n]
    dict = defaultdict(int)
    candidates, distances, hf = _lsh.lsh(data, query, r, a, sd)
    print("Calculated approximate in: " + str(time() - t0))
    for l in range(len(candidates)):
        for k in range(len(candidates[0])):
            for i in range(len(candidates[0][0])):
                dict[candidates[l][k][i]] += distances[l][k][i]
    sorted_dict = {k: v for k, v in sorted(dict.items(), key=lambda item: item[1])}
    candidates = list(sorted_dict.keys())
    print("Calculated ranked in: " + str(time() - t0))

    print(candidates[0:20])

    t0 = time()
    # distances = [dtw_ndim.distance_fast(window, query) for window in data]
    distances = [dtw(window, query, global_constraint="sakoe_chiba", sakoe_chiba_radius=int(0.05 * 120)) for window in data]
    topk_dtw = sorted(range(len(distances)), key=lambda k: distances[k])
    print("Calculated exact dtw in: " + str(time() - t0))
    print(topk_dtw[0:20])

    t0 = time()
    l2distances = [np.linalg.norm(window - query) for window in data]
    print("Calculated exact l2 in: " + str(time() - t0))

    # # distances_ed = [distance.euclidean(query, window) for window in data]
    # # topk_ed = sorted(range(len(distances_ed)), key=lambda k: distances_ed[k])
    #
    accuracy = 0
    for index in topk_dtw[0:20]:
        if index in candidates:
            accuracy += 1
    print(accuracy)
