import os
import unittest
from preprocessing import read_egr_data

os.chdir("..")


class TestPreprocessing(unittest.TestCase):
    def test_read_egr_data(self):
        response = read_egr_data()

        self.assertEqual(list(response[1].keys()), ["index", "values", "name"])
        self.assertAlmostEqual(response[1]["values"][1], 45.73046875, places=3)


if __name__ == '__main__':
    unittest.main()
