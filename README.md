# PSEUDo: Pattern Search, Exploration and Understanding in multivariate time series Data

[![](http://img.youtube.com/vi/oJfXoDyZRPY/0.jpg)](http://www.youtube.com/watch?v=oJfXoDyZRPY "")

## Introduction
**PSEUDo** is an application to explore large multivariate time series and query interesting patterns
while enabling a clear understanding of what's going on behind the scenes on the machine learning side. 
This is achieved by combining the vast knowledge of domain-experts with the immense processing power of computers, 
creating the interactive machine learning tool called **PSEUDo**.

The application consists of three parts:
1. An Angular web application on the client side 
2. A Python Flask backend with REST API on the server side
3. A query-aware locality-sensitive hashing algorithm library in C++

## Setup
To run the application, make sure you've installed the following:
1. Python >=3.5 e.g. from https://www.python.org/downloads/
2. Conda e.g. from https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html
3. Angular: https://angular.io/guide/setup-local
4. Java: https://www.java.com/en/download/help/download_options.html
5. (Only for Windows) MSVC build tool: https://visualstudio.microsoft.com/
6. (Only for Windows) `make` in git Bash: https://gist.github.com/evanwill/0207876c3243bbb6863e65ec5dc3f058 

### Step 1: Create an environment
All dependencies are listed in the *environment.yml* file. To create an environment, run the following command:
`conda env create -f environment.yml`
This will create a conda environment named *pseudo*. Now activate the environment as follows:
`conda activate pseudo`

### Step 2: Set up backend - creating the LSH package
The LSH algorithm is maintained locally for now, so you'll have to create it manually. The file that you need to setup this package is 
located in the backend folder (this is more efficient when debugging, as for every change you have to rebuild the package). 
So the package can be created by running the following code:
`cd backend/libs`
`python setup.py build_ext --inplace && python setup.py install`
`cd ..`
**NOTE 1**: So as a reminder, don't forget to run the 2nd line everytime you change something in the c++ code.

### Step 3: Set up frontend - install Node packages
The cloned Angular application cannot be launched directly. You have to install the node packages via
`cd frontend`
`npm install`

### Step 4: Launch PSEUDo
As mentioned before, PSEUDo has of a user interface and a server. 
A Makefile is provided to setup both easily. 
Just run the following commands from PSEUDo's root directory for the server and ui respectively:
`make server`
`make ui`
A browser window should automatically be opened when running the UI. If not, visit http://localhost:4200/
**NOTE 1**: Both ui and server needs their own terminal window.
**NOTE 2**: Make sure the pseudo environment is activated (as described in step 1) when running the server, 
otherwise you'll get ModuleNotFound errors.

### Step 5: Load data
config.yml configures the global settings in PSEUDo, including the path of the config file of the used dataset.
Some examples of dataset config files can be found under experiments/configs/.

Currently, PSEUDo supports HDF files containing the data plus a JSON file describing the meta-information of the data (espl. the track names).
An example is the combination of data/eeg_eye_state/eeg_eye_state.hdf and data/eeg_eye_state/metadata.json. 
You may want to use [HDFView](https://www.hdfgroup.org/downloads/hdfview/) to explore HDF files and
use the python library [PyTables](https://www.pytables.org/usersguide/tutorials.html) or 
[h5py](https://docs.h5py.org/en/stable/index.html) to convert your data into HDF format.

### Tips and comments
This PSEUDo version is a prototype, which is prone to bugs and insufficient functions. 
It is released for evaluation and knowledge exchange. 
We are working on an upgraded version with augmented functionality and thorough tests. 
However, the open-source issue is still under discussion.

You may find the following tips or comments useful: 

- PSEUDo is designed mainly for high-dimensional time series. Its sublinear scalability refers to the asymptotic complexity with respect to the number of tracks;
- PSEUDo's active learning mainly works for high-dimensional time series. Because the mechanism works by attaching larger weights to important tracks. Though updating query with DBA works also in univariate case;
- The track weights and query will be updated in each feedback round, however, the hash functions are initialized rather than incrementally learnt;
- Only positive labels take effect and negative labels are ignored in this version; 
- This version does not support variable query length. Namely, the patterns searched for in the time series should have similar length as the query;
- You may want to use the same query length because the preprocessed data and the estimated LSH parameters are cached. Especially the latter takes some time.
- The loaded tracks may not scale well vertically. Tuning the range slider rescales it properly.  

# Documentation
## Frontend Views
The UI is backed up by the Angular framework. 
It consists of views (components), a state service and an API service. 
Every time an API call finishes, the state changes. 
Using hooks (EventEmitters), views can subscribe state variable changes and refresh their view with new values. 
The views are listed below.
### Overview
shows the entire dataset. 
Upon receiving labels and predictions, it shows the locations of labels and predictions in this overview.
### Query
shows the current query.
### Training
shows the sampled predictions. 
In this view the user can label the samples as correct or incorrect. 
When the user is satisfied and klick the "Train" button, the labels will be updated and new samples will be generated.
### Progress
shows the progress of the learned classifier. 
It shows whether the classifier is getting better at understanding the desired pattern
### Labels
shows the currently labeled windows. The user should be able to change and delete labels in this view.

## Backend API
### /load-data
reads time series data from a file and returns the values and indices.
### /create-windows
reads time series data and chunks it into windows. For now the windows are saved to a file locally.
### /get-lsh-parameters
calculates the necessary LSH parameters 
- envolope `r`, 
- mean distances of all samples pairs `a` and 
- standard deviation of all sample pairs `sd` 

based on the dataset.
### /initialize
starts the initial iteration of the LSH algorithm. 
First some essential parameters are calculated. 
Then the LSH algorithm is called. 
The API returns the candidates, distances and hash functions.
### /weights
calculates the new weights for the hash function distributions, based on the relevance feedback given by the user.
### /update
runs the LSH algorithm with weights that will manipulate the hashing functions. 
The API returns the candidates, distances and hash functions.
### /query
returns the query data based on the provided window indices. 
If only one index is given, the API call will return the window values according to the index. 
If multiple indices are given, the DBA-based average of the indexed windows is returned.
### /window
simply returns the window values according to the index.
### /table-info
returns extra info needed for the progression view. 
The input will be a subdivision of windows into buckets, 
and for each bucket the prototype (average) will be calculated + the distances between each prototype.
