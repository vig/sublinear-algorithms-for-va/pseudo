#include <Python.h>
#include <numpy/arrayobject.h>
#include "lsh.h"
#include <math.h>

/* Docstrings */
static char module_docstring[] = "This module implements fast nearest-neighbor retrieval using LSH.";
static char lsh_docstring[] = "Calculate the closest neightbours with distances given a query window.";

/* Available functions */
static PyObject* lsh_lsh(PyObject *self, PyObject *args);

/* Module specification */
static PyMethodDef module_methods[] = { { "lsh", lsh_lsh, METH_VARARGS, lsh_docstring }, { NULL, NULL, 0, NULL } };

/* Initialize the module */
#if PY_MAJOR_VERSION >= 3
#define MOD_ERROR_VAL NULL
  #define MOD_SUCCESS_VAL(val) val
  #define MOD_INIT(name) PyMODINIT_FUNC PyInit_##name(void)
  #define MOD_DEF(ob, name, doc, methods) \
          static struct PyModuleDef moduledef = { \
            PyModuleDef_HEAD_INIT, name, doc, -1, methods, }; \
          ob = PyModule_Create(&moduledef);
#else
#define MOD_ERROR_VAL
#define MOD_SUCCESS_VAL(val)
#define MOD_INIT(name) void init##name(void)
#define MOD_DEF(ob, name, doc, methods) \
          ob = Py_InitModule3(name, methods, doc);
#endif

MOD_INIT(_lsh)
{
    PyObject *m;

    MOD_DEF(m, "_lsh", module_docstring,
            module_methods)

    if (m == NULL)
        return MOD_ERROR_VAL;

    import_array();

    return MOD_SUCCESS_VAL(m);
}

static PyObject* lsh_lsh(PyObject* self, PyObject* args) {
    PyObject* data_obj = NULL;
    PyObject* query_obj = NULL;
    PyObject* weights_obj = NULL;
    float *** hashFunctions;
    double * weights = NULL;
    float*** data;
    float ** query;
    int *** candidates;
    double *** distances;
    int nrOfCandidates;
    double r;
    double a;
    double sd;
    int ed;
    PyArray_Descr *descr;
    descr = PyArray_DescrFromType(NPY_DOUBLE);
    PyArray_Descr *descr_float;
    descr_float = PyArray_DescrFromType(NPY_FLOAT);

    printf("Parsing\n");

    /// Parse the input tuple
    if (!PyArg_ParseTuple(args, "OOdddi|O", &data_obj, &query_obj, &r, &a, &sd, &ed, &weights_obj)) {
        return NULL;
    }

    /// Get the dimensions of the data and query
    int data_size = (long) PyArray_DIM(data_obj, 0);
    int channel_size = (long) PyArray_DIM(data_obj, 2);
    int query_size = (int) PyArray_DIM(query_obj, 0);

    printf("Loading data\n");
    /// Convert data, query and weights to C array
    npy_intp dims1[1];
    npy_intp dims2[2];
    npy_intp dims3[3];
    if (PyArray_AsCArray(&query_obj, (void **)&query, dims2, 2, descr_float) < 0 || PyArray_AsCArray(&data_obj, (void ***)&data, dims3, 3, descr_float) < 0) {
        printf("ERROR\n");
        PyErr_SetString(PyExc_TypeError, "error converting to c array");
        return NULL;
    }
    if (weights_obj != NULL)
    {
        printf("Using weights");
        if (PyArray_AsCArray(&weights_obj, (void *)&weights, dims1, 1, descr) < 0) {
            PyErr_SetString(PyExc_TypeError, "error converting weights to c array");
            return NULL;
        }
    }

    int K = ceil(log((log(0.5))/log(1-exp(-2*(0.1)*(0.1)*query_size)))/log((1-exp(-2*(0.1)*(0.1)*query_size))/(0.5)));
    int L = ceil(log(0.05)/(log(1-pow(1-exp(-2*(0.1)*(0.1)*query_size), K))));
    printf("K: %d\n", K);
    printf("L: %d\n", L);
    printf("Dim: %d\n", channel_size);

    /// Initialize output parameters
    hashFunctions = (float ***)malloc(L*sizeof(float**));
    for (int l=0;l<L;l++)
    {
        hashFunctions[l] = (float **)malloc(K*sizeof(float*));
        for (int k=0;k<K;k++)
        {
            hashFunctions[l][k] = (float *)malloc(channel_size*sizeof(float));
        }
    }

    int status = lsh(data, data_size, query_size, channel_size, query, L, K, r, a, sd, ed, candidates, distances, hashFunctions, weights, nrOfCandidates);
    if (status) {
        PyErr_SetString(PyExc_RuntimeError, "lsh could not allocate memory");
        return NULL;
    }

    npy_intp dimscandidates[3] = {L, K, nrOfCandidates};
    printf("Number of candidates: %d\n", nrOfCandidates);
    npy_intp dims4[4] = {L, K, channel_size};

    PyArrayObject* numpy_candidates = (PyArrayObject*)PyArray_SimpleNew(3, dimscandidates, NPY_INT);
    int* numpy_candidates_data = (int*)PyArray_DATA(numpy_candidates);
    for (int l=0;l<L;l++)
    {
        for (int k=0;k<K;k++)
        {
            memcpy(numpy_candidates_data, candidates[l][k], nrOfCandidates*sizeof(int));
            numpy_candidates_data += nrOfCandidates;
        }
    }

    PyArrayObject* numpy_distances = (PyArrayObject*)PyArray_SimpleNew(3, dimscandidates, NPY_DOUBLE);
    double* numpy_distances_data = (double*)PyArray_DATA(numpy_distances);
    for (int l=0;l<L;l++)
    {
        for (int k=0;k<K;k++)
        {
            memcpy(numpy_distances_data, distances[l][k], nrOfCandidates*sizeof(double));
            numpy_distances_data += nrOfCandidates;
        }
    }

    PyArrayObject* numpy_hash_functions = (PyArrayObject*)PyArray_SimpleNew(3, dims4, NPY_FLOAT);
    float* numpy_hash_functions_data = (float*)PyArray_DATA(numpy_hash_functions);
    for (int l=0;l<L;l++)
    {
        for (int k=0;k<K;k++)
        {
            memcpy(numpy_hash_functions_data, hashFunctions[l][k], channel_size*sizeof(float));
            numpy_hash_functions_data += channel_size;
        }
    }
    PyObject* ret = Py_BuildValue("NNN", PyArray_Return(numpy_candidates), PyArray_Return(numpy_distances), PyArray_Return(numpy_hash_functions));
    return ret;
}
