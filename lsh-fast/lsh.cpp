#include <iostream>     // std::cout
#include <algorithm>    // std::max
#include <fstream>
#include <string.h>
#include <tuple>
#include <stdlib.h>
#include <vector>
#include <math.h>
#include <random>

#define INF 1e20 // Pseudo-infinite number for this code
#define dist(x,y) ((x-y)*(x-y))
//#define dist(x,y) (abs(x-y))

using namespace std;

double l2(float* A, float* B, int m) {
    float distance = 0;
    for (int i = 0; i < m; i++) {
        distance += std::abs(A[i]-B[i]);
    }
    return distance;
}

/// This function is from the UCR Suite code: https://www.cs.ucr.edu/~eamonn/UCRsuite.html
///
/// Calculate Dynamic Time Wrapping distance
/// A,B: data and query, respectively
/// cb : cummulative bound used for early abandoning
/// r  : size of Sakoe-Chiba warpping band
double dtw(float* A, float* B, double *cb, int m, int r, double best_so_far) {
    double *cost;
    double *cost_prev;
    double *cost_tmp;
    int i, j, k;
    double x, y, z, min_cost;

    /// Instead of using matrix of size O(m^2) or O(mr), we will reuse two arrays of size O(r).
    cost = (double*) calloc(2 * r + 1, sizeof(double));
    cost_prev = (double*) calloc(2 * r + 1, sizeof(double));
    for (k = 0; k < 2 * r + 1; k++) {
        cost[k] = INF;
        cost_prev[k] = INF;
    }

    for (i = 0; i < m; i++) {
        k = max(0, r - i);
        min_cost = INF;

        for (j = max(0, i - r); j <= min(m - 1, i + r); j++, k++) {
            /// Initialize all row and column
            if ((i == 0) && (j == 0)) {
                cost[k] = dist(A[0], B[0]);
                min_cost = cost[k];
                continue;
            }

            if ((j - 1 < 0) || (k - 1 < 0)) {
                y = INF;
            } else {
                y = cost[k - 1];
            }
            if ((i - 1 < 0) || (k + 1 > 2 * r)) {
                x = INF;
            } else {
                x = cost_prev[k + 1];
            }
            if ((i - 1 < 0) || (j - 1 < 0)) {
                z = INF;
            } else {
                z = cost_prev[k];
            }

            /// Classic DTW calculation
            cost[k] = min( min( x, y) , z) + dist(A[i], B[j]);

            /// Find minimum cost in row for early abandoning (possibly to use column instead of row).
            if (cost[k] < min_cost) {
                min_cost = cost[k];
            }
        }

        /// Move current array to previous array.
        cost_tmp = cost;
        cost = cost_prev;
        cost_prev = cost_tmp;
    }
    k--;

    /// the DTW distance is in the last cell in the matrix of size O(m^2) or at the middle of our array.
    double final_dtw = cost_prev[k];
    free(cost);
    free(cost_prev);
    return final_dtw;
}

/// Sorting function for the 2nd element of two vectors
// sort by second ascending
bool sortbysecasc(const pair<int,double> &a,
                   const pair<int,double> &b)
{
    return a.second<b.second;
}

/// Sorting function for the 2nd element of two vectors
// Sort by second descending
bool sortbysecdesc(const pair<int,double> &a,
                   const pair<int,double> &b)
{
    return a.second>b.second;
}

/// Get random value from uniform distribution
double uniform(double a, double b)
{
    return (double) std::rand() / (RAND_MAX) * (b - a) + a;
}

/// Algorithm for calculating similarity of (multivariate) time series data
int lsh(float *** D, int M, int T, int Dim, float ** Q, int L, int K, double r, double a, double sd, int ed, int *** &candidates, double *** &distances, float *** &hashFunctions, double * weights, int &nrOfCandidates)
{
    clock_t begin_time = clock();
    double time_spent = 0;
    clock_t end_time = clock();
    std::random_device rd{};
    std::mt19937 gen{rd()};
    std::normal_distribution<> distribution0{0,1};
    srand(time(NULL));

    int S = 2; //3
    double delta = r * 0.75; //0.07 => 20.000  maximum allowed false negative rate
    int threshold = T * 0.22; //0.8 => 18/67  minimum number of project collisions for a hash collision

    /// Create hash functions
    float value;
    for (int l=0;l<L;l++)   // compound hash functions H_G
    {
        for (int k=0;k<K;k++)   // hash functions H
        {
            for (int d=0;d<Dim;d++)
            {
                value = distribution0(gen);
                if (weights == NULL)
                {
                    hashFunctions[l][k][d] = (float)value;
                } else {
                    hashFunctions[l][k][d] = (float)(value * weights[d]);
                }
            }
        }
    }
    end_time = clock();
    time_spent = (double)(end_time - begin_time) / CLOCKS_PER_SEC;
    std::cout << "Hash functions created: " << time_spent << " seconds" << std::endl;

    /// Create hashSET
    float temp0 = 0;
    float **** hashSET = (float****)malloc(M*sizeof(float***)) ;
    for (int m=0;m<M;m++)
    {
        hashSET[m] = (float***)malloc(L*sizeof(float**)) ;
        for (int l=0;l<L;l++)
        {
            hashSET[m][l] = (float**)malloc(K*sizeof(float*)) ;
            for (int k=0;k<K;k++)
            {
                hashSET[m][l][k] = (float*)malloc(T*sizeof(float)) ;
                for (int t=0;t<T;t++)
                {
                    temp0 = 0;
                    for (int d=0;d<Dim;d++) {
                        temp0 += hashFunctions[l][k][d] * D[m][t][d];
                    }
                    hashSET[m][l][k][t] = temp0;
                }
            }
        }
    }
    end_time = clock();
    time_spent = (double)(end_time - begin_time) / CLOCKS_PER_SEC;
    std::cout << "Set hashed: " << time_spent << " seconds" << std::endl;

    /// Create upper and lower bound on query
    float ** Q_U = (float**)malloc((T)*sizeof(float*));
    float ** Q_L = (float**)malloc((T)*sizeof(float*));
    for (int t=0;t<T;t++)
    {
        Q_U[t] = (float*)malloc((Dim)*sizeof(float));
        Q_L[t] = (float*)malloc((Dim)*sizeof(float));
    }
    float currentMax = -10000;
    float currentMin = 10000;
    for (int t = 0; t < T; t++) {
        for (int d = 0; d < Dim; d++) {
            for (int s = -S; s <= S; s++) {
                if (t + s < 0 || t + s > T - 1)
                    continue;
                currentMax = std::max(Q_U[t + s][d], currentMax);
                currentMin = std::min(Q_L[t + s][d], currentMin);
            }
            Q_U[t][d] = currentMax;
            Q_L[t][d] = currentMin;
            currentMax = -10000;
            currentMin = 10000;
        }
    }
    end_time = clock();
    time_spent = (double)(end_time - begin_time) / CLOCKS_PER_SEC;
    std::cout << "Query upper and lower bound: " << time_spent << " seconds" << std::endl;

    float temp1 = 0;
    float temp2 = 0;
    float temp3 = 0;

    /// Create query hashes
    float *** hashQ = (float***)malloc(L*sizeof(float**)) ;
    float *** hashQ_U = (float***)malloc(L*sizeof(float**)) ;
    float *** hashQ_L = (float***)malloc(L*sizeof(float**)) ;
    for (int l=0;l<L;l++)
    {
        hashQ[l] = (float**)malloc(K*sizeof(float*)) ;
        hashQ_U[l] = (float**)malloc(K*sizeof(float*)) ;
        hashQ_L[l] = (float**)malloc(K*sizeof(float*)) ;

        for (int k=0;k<K;k++)
        {
            hashQ[l][k] = (float*)malloc(T*sizeof(float)) ;
            hashQ_U[l][k] = (float*)malloc(T*sizeof(float)) ;
            hashQ_L[l][k] = (float*)malloc(T*sizeof(float)) ;
            for (int t=0;t<T;t++)
            {
                temp1 = 0;
                temp2 = 0;
                temp3 = 0;
                for (int d=0;d<Dim;d++)
                {
                    temp1 += hashFunctions[l][k][d] * Q[t][d];
                    temp2 += hashFunctions[l][k][d] * Q_U[t][d];
                    temp3 += hashFunctions[l][k][d] * Q_L[t][d];
                }
                hashQ[l][k][t] = temp1;
                hashQ_U[l][k][t] = temp2;
                hashQ_L[l][k][t] = temp3;
            }
        }
    }
    end_time = clock();
    time_spent = (double)(end_time - begin_time) / CLOCKS_PER_SEC;
    std::cout << "Query hashed: " << time_spent << " seconds" << std::endl;

    /// Generate candidates
    vector<int> v;
    v.reserve(M);
    bool group_collision;
    int hash_collision;
    int projection_collision;
    for (int m=0;m<M;m++)
    {
        group_collision = false;
        for (int l=0;l<L;l++)
        {
            hash_collision = 0;
            for (int k=0;k<K;k++)
            {
                projection_collision = 0;
                for (int t=0;t<T;t++)
                {
                    if (std::abs(hashQ[l][k][t]-hashSET[m][l][k][t]) <= delta/2 ||
                    std::abs(hashQ_U[l][k][t]-hashSET[m][l][k][t]) <= delta/2 ||
                    std::abs(hashQ_L[l][k][t]-hashSET[m][l][k][t]) <= delta/2
                            )
                    {
                        projection_collision++;
                    }
                }
                if (projection_collision>=threshold)
                {
                    hash_collision++;
                }
                else
                    break;
            }
            if (hash_collision==K)
            {
                group_collision = true;
            }
        }
        if (group_collision)
        {
            v.emplace_back(m);
        }
    }
    end_time = clock();
    time_spent = (double)(end_time - begin_time) / CLOCKS_PER_SEC;
    std::cout << "Hashing done in: " << time_spent << " seconds" << std::endl;
    std::cout << "Number of candidates pruned: " << 100 - (100 * v.size() / M) << "%" << std::endl;

    /// Calculate distance on compressed MST data
    vector<pair<int, double>> dtwsim;
    dtwsim.reserve(v.size());

    double distance = 0;
    candidates = (int***)malloc(L*sizeof(int**));
    distances = (double***)malloc(L*sizeof(double**));
    for (int l=0;l<L;l++)
    {
        candidates[l] = (int**)malloc(K*sizeof(int*));
        distances[l] = (double**)malloc(K*sizeof(double*));
        for (int k = 0; k < K; k++)
        {
            candidates[l][k] = (int*)malloc(v.size()*sizeof(int));
            distances[l][k] = (double*)malloc(v.size()*sizeof(double));
            dtwsim.clear();
            for (int m=0;m<v.size();m++)
            {
                if (ed == 0) {
                    distance = dtw(hashSET[v[m]][l][k], hashQ[l][k], NULL, T, 0.05 * 120, INF);
                } else {
                    distance = l2(hashSET[v[m]][l][k], hashQ[l][k], T);
                }
                dtwsim.emplace_back(v[m], distance);
            }
            sort(dtwsim.begin(), dtwsim.end(), sortbysecasc);
            for (int i=0; i<dtwsim.size(); i++)
            {
                candidates[l][k][i] = dtwsim[i].first;
                distances[l][k][i] = dtwsim[i].second;
            }
        }
    }
    end_time = clock();
    time_spent = (double)(end_time - begin_time) / CLOCKS_PER_SEC;
    std::cout << "DTW sim done in: " << time_spent << std::endl;

    nrOfCandidates = dtwsim.size();

    /// Clean up
    v.clear();
    dtwsim.clear();
    for (int t=0;t<T;t++)
    {
        free(Q_U[t]);
        free(Q_L[t]);
    }
    free(Q_U);
    free(Q_L);
    for (int m=0;m<M;m++)
    {
        for (int l=0;l<L;l++)
        {
            for (int k=0;k<K;k++)
            {
                free(hashSET[m][l][k]);
            }
            free(hashSET[m][l]);
        }
        free(hashSET[m]);
    }
    free(hashSET);
    for (int l=0;l<L;l++)
    {
        for (int k=0;k<K;k++)
        {
            free(hashQ[l][k]);
            free(hashQ_L[l][k]);
            free(hashQ_U[l][k]);
        }
        free(hashQ[l]);
        free(hashQ_U[l]);
        free(hashQ_L[l]);
    }
    free(hashQ);
    free(hashQ_U);
    free(hashQ_L);
    return 0;
}

int main() {
    /// Run this function for debugging
    srand((unsigned int)time(NULL));
    clock_t begin_time = clock();
    FILE *data;
    const int M = 125621;
    int K = 2; //ln((ln(0.5))/ln(1-exp(-2*(0.1)^2*120)))/ln((1-exp(-2*(0.1)^2*120))/(0.5))
    int L = 6; //ln(0.05)/(ln(1-(1-exp(-2(0.1)^2*120))^4))0.0653330009864134
    int T = 120;
    int Dim = 1;

    /// Read data
    float *** D = (float***)malloc(M*sizeof(float**)) ;
    for (int i=0;i<M;i++)
    {
        D[i] = (float**)malloc((T)*sizeof(float*));
        for (int t=0;t<T;t++)
        {
            D[i][t] = (float*)malloc((Dim)*sizeof(float));
        }
    }
    data = fopen("processed-data","r");
    if( data == NULL )
        exit(2);
    char d[M];
    int i; int j = 0; float f;
    while(fscanf(data,"%[^\n]\n",d) != EOF && j< M)
    {
        char *p =  strtok(d," ");
        i = 0;
        while (p != NULL && i<T)
        {
            f = atof(p);
            for (int d=0;d<Dim;d++)
            {
                D[j][i][d] = f;
            }
            i++;
            p = strtok (NULL, " ");
        }
        j++;
    }

    /// Print time
    clock_t end_time = clock();
    double time_spent = (double)(end_time - begin_time) / CLOCKS_PER_SEC;
    std::cout << "Data read in: " << time_spent << std::endl;
    begin_time = clock();

    /// Create query
    float ** Q = (float**)malloc((T)*sizeof(float*));
    for (int t=0;t<T;t++)
    {
        Q[t] = (float*)malloc((Dim)*sizeof(float));
        for (int d=0;d<Dim;d++)
        {
            Q[t][d] = D[80503][t][d];
        }
    }

    int testt = 0;
    while (testt < 5) {
        /// Initialize output variables
        int ***candidates;
        int nrOfCandidates;
        double ***distances;
        float ***hashFunctions = (float ***) malloc(L * sizeof(float **));
        for (int l = 0; l < L; l++) {
            hashFunctions[l] = (float **) malloc(K * sizeof(float *));
            for (int k = 0; k < K; k++) {
                hashFunctions[l][k] = (float *) malloc(Dim * sizeof(float));
            }
        }
        double a = 3.1549093441462044;
        double sd = 0.514583453532817;
        double r = 0.21020966674727637;

        lsh(D, M, T, Dim, Q, L, K, r, a, sd, 0, candidates, distances, hashFunctions, NULL, nrOfCandidates);

        cout << "Top-10 candidates: ";
        for (int i=0; i<10; i++)
        {
            cout << candidates[i] << ",";
        }
        cout << endl;

//        for (int l = 0; l < L; l++) {
//            for (int k = 0; k < K; k++) {
//                for (int t = 0; t < T; t++) {
//                    free(hashFunctions[l][k][t]);
//                }
//                free(hashFunctions[l][k]);
//            }
//            free(hashFunctions[l]);
//        }
//        free(hashFunctions);
//        cout << endl;
//        free(candidates);
//        free(distances);

        testt++;
    }


    end_time = clock();
    time_spent = (double)(end_time - begin_time) / CLOCKS_PER_SEC;
    std::cout << "Query done in: " << time_spent << std::endl;

    return 0;
}