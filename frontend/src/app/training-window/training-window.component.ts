import { Component, OnInit } from '@angular/core';
import {StateService} from '../state.service';

@Component({
  selector: 'app-training-window',
  templateUrl: './training-window.component.html',
  styleUrls: ['./training-window.component.css']
})
export class TrainingWindowComponent implements OnInit {
  public labels: boolean[] = [];
  public hashFunctionLabels: boolean[] = [];

  constructor(private state: StateService) { }

  ngOnInit(): void {
  }

  async train() {
    this.state.labels = Object.assign({}, this.state.labels, this.labels);
    await this.state.getQueryWindow(this.state.labels, null, null);
    const hashFunctions: number[][] = [];
    this.hashFunctionLabels.forEach((label, index) => {
      if (label) {
        hashFunctions.push(this.state.lshData.hash_functions[index]);
      }
    });
    await this.state.update(Object.assign({}, this.labels), hashFunctions);
  }

  public get show() {
    return !!this.state.lshData;
  }

  changeTab(tab) {
    this.state.currentTab = tab.index;
  }
}
