import { Component, OnInit } from '@angular/core';
import {StateService} from '../state.service';

@Component({
  selector: 'app-query-window',
  templateUrl: './query-window.component.html',
  styleUrls: ['./query-window.component.css']
})
export class QueryWindowComponent implements OnInit {

  public plot;

  constructor(private state: StateService) { }

  ngOnInit(): void {
    this.state.onNewQuery.subscribe(() => {
      if (this.state.queryWindow) {
        console.log(this.state.queryWindow);
        this.initializePlot();
      }
    });
    this.state.onNewSelection.subscribe(() => {
      if (this.state.queryWindow) {
        this.updateChannels();
      }
    });
  }

  initializePlot(): void {
    console.log('selection');
    console.log(this.state.selection);
    const subplots = [];
    const data = [];
    this.state.selection.forEach((channelIndex: number, index: number) => {
      console.log(channelIndex);
      const channel = this.state.queryWindow[channelIndex];
      data.push({
        x: [...Array(channel.length).keys()],
        y: channel,
        type: 'line',
        xaxis: 'x',
        yaxis: `y${index + 2}`,
        line: {
          color: this.state.colors[channelIndex],
        }
      });
      subplots.push([`xy${index + 2}`]);
    });

    const plot = {
      data: data,
      layout: {
        grid: {
          rows: this.state.selection.length,
          columns: 1,
          subplots: subplots,
        },
        showlegend: false,
        hovermode: 'closest',
        autosize: true,
        margin: {
          l: 30,
          r: 30,
          t: 30,
          b: 0,
          pad: 4
        },
        height: 100 * this.state.selection.length,
        width: 300,
        titlefont: {
          size: 9
        },
        xaxis: {
          showgrid: false,
          zeroline: false,
          showticklabels: false,
        },
        yaxis: {
          zeroline: false,
          showticklabels: false,
        }
      }
    };
    this.state.selection.forEach((channel: number, index: number) => {
      plot.layout[`yaxis${index + 2}`] = {
        zeroline: false,
        showticklabels: false,
        annotations: [{
          xref: 'paper',
          yref: 'paper',
          x: 0,
          xanchor: 'right',
          y: 1,
          yanchor: 'bottom',
          text: 'X axis label',
          showarrow: false
        }]
        // title: this.state.rawData[channel].name
      };
    });
    this.plot = plot;
  }

  async updateChannels() {
    const subplots = [];
    const data = [];
    this.state.selection.forEach((channelIndex: number, index: number) => {
      const channel = this.state.queryWindow[channelIndex];
      data.push({
        x: [...Array(channel.length).keys()],
        y: channel,
        type: 'line',
        xaxis: 'x',
        yaxis: `y${index + 2}`,
        line: {
          color: this.state.colors[channelIndex],
        }
      });
      subplots.push([`xy${index + 2}`]);
    });

    const plot = {
      data: data,
      layout: {
        grid: {
          rows: this.state.selection.length,
          columns: 1,
          subplots: subplots,
        },
        showlegend: false,
        hovermode: 'closest',
        autosize: true,
        margin: {
          l: 30,
          r: 30,
          t: 30,
          b: 0,
          pad: 4
        },
        height: 100 * this.state.selection.length,
        width: 300,
        titlefont: {
          size: 9
        },
        xaxis: {
          showgrid: false,
          zeroline: false,
          showticklabels: false,
        },
        yaxis: {
          zeroline: false,
          showticklabels: false,
        }
      }
    };
    this.state.selection.forEach((channel: number, index: number) => {
      plot.layout[`yaxis${index + 2}`] = {
        zeroline: false,
        showticklabels: false,
      };
    });
    this.plot = plot;
  }

    get isQuerySet(): boolean {
    return !!this.state.queryWindow && !!this.plot;
  }
}
