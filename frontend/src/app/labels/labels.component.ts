import { Component, OnInit } from '@angular/core';
import { StateService } from '../state.service';

@Component({
  selector: 'app-labels',
  templateUrl: './labels.component.html',
  styleUrls: ['./labels.component.css']
})
export class LabelsComponent implements OnInit {

  public goodLabels = [];
  public badLabels = [];

  constructor(private state: StateService) { }

  ngOnInit(): void {
    this.state.onNewLabels.subscribe(() => { this.createSubplots(); });
    this.state.onNewSelection.subscribe(() => { this.createSubplots(); });
  }

  async createSubplots() {
    this.goodLabels = [];
    this.badLabels = [];
    const labelWindows: number[][][] = await this.state.getWindow(Object.keys(this.state.labels).map(Number));
    for (const idx in Object.keys(this.state.labels).map(Number)) {
      const window = labelWindows[idx];
      const data = [];
      this.state.selection.forEach((channelIndex, index) => {
        const channel = window[channelIndex];
        data.push({
          x: [...Array(channel.length).keys()],
          y: channel,
          type: 'line',
          xaxis: 'x',
          yaxis: `y${index + 2}`,
        });
      });
      const subplots = [];
      this.state.selection.forEach((channelIndex, index) => {
        subplots.push([`xy${index + 2}`]);
      });
      const plot = {
        index: Object.keys(this.state.labels).map(Number)[idx],
        data: data,
        layout: {
          grid: {
            rows: this.state.selection.length,
            columns: 1,
            subplots: subplots,
          },
          showlegend: false,
          hovermode: 'closest',
          autosize: true,
          margin: {
            l: 30,
            r: 30,
            t: 30,
            b: 0,
            pad: 4
          },
          height: 100 * this.state.selection.length,
          width: 300,
          titlefont: {
            size: 9
          },
          xaxis: {
            showgrid: false,
            zeroline: false,
            showticklabels: false,
          },
          yaxis: {
            zeroline: false,
            showticklabels: false,
          }
        }
      };
      this.state.selection.forEach((channelIndex, index) => {
        plot.layout[`yaxis${index + 2}`] = {
          zeroline: false,
          showticklabels: false,
        };
      });
      if (this.state.labels[Object.keys(this.state.labels).map(Number)[idx]]) {
        this.goodLabels.push(plot);
      } else {
        this.badLabels.push(plot);
      }
    }
  }
}
