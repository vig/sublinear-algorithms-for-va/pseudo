import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {StateService} from '../state.service';

@Component({
  selector: 'app-table-overview',
  templateUrl: './table-overview.component.html',
  styleUrls: ['./table-overview.component.css']
})
export class TableOverviewComponent implements OnInit {
  @Output() labelsOutput = new EventEmitter<boolean[]>();
  public subplots;
  public averages;
  public layout;
  public labels: boolean[];
  public prototypes;
  constructor(private state: StateService) { }

  ngOnInit(): void {
    this.state.onNewLshData.subscribe(() => {
      this.createHistograms();
      this.createPrototypes();
    });
    this.state.onNewSelection.subscribe(() => {
      this.updateChannels();
    });
  }

  public labelCorrect(index: number) {
    this.labels[index] = true;
    this.labelsOutput.emit(this.labels);
  }

  public labelUndefined(index: number) {
    if (this.labels[index] !== undefined) {
      delete this.labels[index];
      this.labelsOutput.emit(this.labels);
    }
  }

  async createPrototypes(): Promise<void> {
    const representatives: number[][] = [];
    this.state.lshData.candidates.forEach((grouphash) => {
      grouphash.forEach((candidates) => {
        representatives.push(candidates.slice(0, 20));
      });
    });
    this.prototypes = await this.state.getTableInfo(representatives);
    console.log(this.prototypes);
    const subplots = [];
    this.averages = this.prototypes.prototypes.map((prototype) => {
      const channelData = [];
      this.state.selection.forEach((channelIndex, index) => {
        const channel = prototype.max[channelIndex];
        channelData.push({
          x: [...Array(channel.length).keys()],
          y: channel,
          xaxis: 'x',
          yaxis: `y${index + 2}`,
          type: 'scatter',
          fill: null,
          mode: 'lines',
          line: {
            color: this.state.colors[channelIndex],
            width: 3
          }
        });
      });
      this.state.selection.forEach((channelIndex, index) => {
        const channel = prototype.min[channelIndex];
        channelData.push({
          x: [...Array(channel.length).keys()],
          y: channel,
          xaxis: 'x',
          yaxis: `y${index + 2}`,
          type: 'scatter',
          fill: 'tonexty',
          mode: 'lines',
          line: {
            color: this.state.colors[channelIndex],
            width: 3
          }
        });
      });
      this.state.selection.forEach((channelIndex, index) => {
        const channel = prototype.average[channelIndex];
        channelData.push({
          x: [...Array(channel.length).keys()],
          y: channel,
          xaxis: 'x',
          yaxis: `y${index + 2}`,
          type: 'line',
          line: {
            color: 'red',
            width: 3
          }
        });
      });
      return channelData;
    });
    for (let index = 0; index < this.state.selection.length; index++) {
      subplots.push([`xy${index + 2}`]);
    }
    this.layout = {
      grid: {
        rows: this.state.selection.length,
        columns: 1,
        subplots: subplots,
      },
      showlegend: false,
      hovermode: 'closest',
      autosize: true,
      margin: {
        l: 10,
        r: 10,
        t: 30,
        pad: 4
      },
      xaxis: {
        showgrid: false,
        zeroline: false,
        showticklabels: false,
      },
      yaxis: {
        zeroline: false,
        showticklabels: false,
      },
      height: 200 * this.state.selection.length,
      width: screen.width * 0.1,
    };
    this.state.selection.forEach((channelIndex, index) => {
      this.layout[`yaxis${index + 2}`] = {
        zeroline: false,
        showticklabels: false,
      };
    });
  }

  async updateChannels() {
    if (!this.prototypes) {
      return;
    }
    const subplots = [];
    this.averages = this.prototypes.prototypes.map((prototype) => {
      const channelData = [];
      this.state.selection.forEach((channelIndex, index) => {
        const channel = prototype.max[channelIndex];
        channelData.push({
          x: [...Array(channel.length).keys()],
          y: channel,
          xaxis: 'x',
          yaxis: `y${index + 2}`,
          type: 'scatter',
          fill: null,
          mode: 'lines',
          line: {
            color: this.state.colors[channelIndex],
            width: 3
          }
        });
      });
      this.state.selection.forEach((channelIndex, index) => {
        const channel = prototype.min[channelIndex];
        channelData.push({
          x: [...Array(channel.length).keys()],
          y: channel,
          xaxis: 'x',
          yaxis: `y${index + 2}`,
          type: 'scatter',
          fill: 'tonexty',
          mode: 'lines',
          line: {
            color: this.state.colors[channelIndex],
            width: 3
          }
        });
      });
      this.state.selection.forEach((channelIndex, index) => {
        const channel = prototype.average[channelIndex];
        channelData.push({
          x: [...Array(channel.length).keys()],
          y: channel,
          xaxis: 'x',
          yaxis: `y${index + 2}`,
          type: 'line',
          line: {
            color: 'red',
            width: 3
          }
        });
      });
      return channelData;
    });
    for (let index = 0; index < this.state.selection.length; index++) {
      subplots.push([`xy${index + 2}`]);
    }
    this.layout = {
      grid: {
        rows: this.state.selection.length,
        columns: 1,
        subplots: subplots,
      },
      showlegend: false,
      hovermode: 'closest',
      autosize: true,
      margin: {
        l: 10,
        r: 10,
        t: 30,
        pad: 4
      },
      xaxis: {
        showgrid: false,
        zeroline: false,
        showticklabels: false,
      },
      yaxis: {
        zeroline: false,
        showticklabels: false,
      },
      height: 200 * this.state.selection.length,
      width: screen.width * 0.1,
    };
    this.state.selection.forEach((channelIndex, index) => {
      this.layout[`yaxis${index + 2}`] = {
        zeroline: false,
        showticklabels: false,
      };
    });
  }

  async createHistograms() {
    this.labels = [];
    console.log('creating table histograms');
    this.subplots = [];
    this.averages = [];
    const tables = this.state.lshData.tables;
    console.log('start of table histograms');
    tables.forEach((table, index) => {
      this.subplots.push(
        {
          index,
          data: [{
            x: Object.keys(table),
            y: Object.values(table).map((values: number[]) => values.length), // / (this.service.rawValues.length - this.service.windowSize)),
            type: 'bar',
            opacity: 0.5,
            marker: {
              color: Object.keys(table).map((key) => {
                return this.getColor(Number(key) / Number(Object.keys(table)[Object.keys(table).length - 1]));
              })
            }
          }],
          layout: {
            hovermode: 'closest',
            autosize: true,
            margin: {
              l: 10,
              r: 10,
              t: 10,
              b: 10,
              pad: 4
            },
            xaxis: {
              showticklabels: false
            },
            yaxis: {
              showticklabels: false
            },
            height: 100,
            width: screen.width * 0.1,
          }
        }
      );
    });
    console.log('tables histogram created');
  }

  public get tables() {
    return this.state.lshData.tables;
  }

  getColor(value) {
    const hue=((1-value)*120).toString(10);
    return ["hsl(",hue,",100%,50%)"].join("");
  }
}
