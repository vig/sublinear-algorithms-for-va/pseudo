import { Injectable } from '@angular/core';

// This interface is called RawDate by Dylan
export interface Channel {
  index: string[];
  values: number[];
  name?: string;
}

export interface LshData {  // LSH results
  candidates: number[][][];     // [l][k][t]
  distances: number[][][];      // [l][k][t]
  average_candidates: number[]; // [t]
  average_distances: number[];  // [t]
  tables: {[bucket: string]: number[]}[];
  average_table: {[bucket: string]: number[]};
  samples: number[];
  hash_functions: number[][];   // [?][d]
  prototype: {
    average: number[][]; // [d][t]
    min: number[][]; // [d][t]
    max: number[][]; // [d][t]
  };
  parameters?: number[];
}

export interface TableInfoData {
  prototypes: {
    average: number[][];
    min: number[][];
    max: number[][];
  }[];
  distances: number[][];  // [x][x]
}

export interface Parameters {
  windowsize: number;
  hashsize: number;
  tablesize: number;
  stepsize: number;
}

export interface State {
  lshData: LshData;
  queryWindow: number[][];  // [d][t]
  weights: number[];        // [d]
  labels: any;              // [?]
}

export interface StateNode {
  id: number;
  image: any;
  state: State;
  children: number[];
}

@Injectable({
  providedIn: 'root'
})
/**
 * This service acts as the interface between the client and server side.
 */
export class ApiService {

  constructor() { }

  /**
   * Read input data. The format is a list of channels, where each channel is an object of type RawData
   */
  async loadData(): Promise<Channel[]> {
    const response = await fetch('http://127.0.0.1:5000/load-data');
    return await response.json();
  }

  /**
   * Split the data into windows (server side)
   */
  async createWindows(parameters: Parameters): Promise<any> {
    const postData = {parameters};
    await fetch('http://127.0.0.1:5000/create-windows', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(postData)
    });
  }
  /**
   * Do the first iteration of LSH and return important information
   */
  async lshInitial(query: number[][]): Promise<LshData> {
    const response = await fetch('http://127.0.0.1:5000/initialize', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: new Blob( [ JSON.stringify({query}) ], { type: 'text/plain' } )
    });
    return await response.json();
  }

  /**
   * Calculate the LSH parameters
   */
  async getParameters(windowsize): Promise<number[]> {
    const response = await fetch('http://127.0.0.1:5000/get-lsh-parameters', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: new Blob( [ JSON.stringify({windowsize}) ], { type: 'text/plain' } )
    });
    return await response.json();
  }

  /**
   * Do another iteration of LSH, with weights, and return important information
   */
  async lshUpdate(query, weights, parameters): Promise<LshData> {
    const response = await fetch('http://127.0.0.1:5000/update', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: new Blob( [ JSON.stringify({query, weights, parameters}) ], { type: 'text/plain' } )
    });
    return await response.json();
  }


  /**
   * Get weights which will be applied to the LSH hash functions
   */
  async getWeights(query: number[][], labels: {[index: number]: boolean}, weights: number[], hash_functions: number[][]): Promise<number[]> {
    const response = await fetch('http://127.0.0.1:5000/weights', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: new Blob( [ JSON.stringify({query, labels, weights, hash_functions}) ], { type: 'text/plain' } )
    });
    return await response.json();
  }

  /**
   * Get query window based on windows labeled correct
   */
  async getQueryWindow(indices: number | {[index: number]: boolean}, query_size: number, start_index:number): Promise<number[][]> {
    const response = await fetch('http://127.0.0.1:5000/query', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({indices, query_size, start_index})
    });
    return await response.json();
  }

  /**
   * Get data of a window by indices
   */
  async getWindowByIndices(indices: number[]): Promise<number[][][]> {
    const response = await fetch('http://127.0.0.1:5000/window', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({indices})
    });
    return await response.json();
  }

  /**
   * Get additional information for a given table
   */
  async getTableInfo(table: number[][]): Promise<TableInfoData> {
    const response = await fetch('http://127.0.0.1:5000/table-info', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({table})
    });
    return await response.json();
  }
}
