import { Component } from '@angular/core';
import { StateService } from '../state.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent {

  constructor(private state: StateService) {
  }

  public get greyOut() {
    return this.state.loadingText !== '';
  }

  public get loadingText(): string {
    return this.state.loadingText.toUpperCase();
  }
}
