import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {StateService} from '../state.service';

@Component({
  selector: 'app-labeling-window',
  templateUrl: './labeling-window.component.html',
  styleUrls: ['./labeling-window.component.css']
})
export class LabelingWindowComponent implements OnInit {
  public topk: number[];
  public subplots = [];
  public labels: boolean[] = [];
  private k = 5;
  @Output() labelsOutput = new EventEmitter<boolean[]>();

  constructor(private state: StateService) { }

  ngOnInit(): void {
    this.state.onNewLshData.subscribe(() => this.showSamples());
    this.state.onNewSelection.subscribe(() => {
      if (this.topk) {
        this.updateChannels();
      }
    });
  }

  public labelCorrect(index: number) {
    this.labels[index] = true;
    this.labelsOutput.emit(this.labels);
  }

  public labelUndefined(index: number) {
    if (this.labels[index] !== undefined) {
      delete this.labels[index];
      this.labelsOutput.emit(this.labels);
    }
  }

  public labelIncorrect(index: number) {
    this.labels[index] = false;
    this.labelsOutput.emit(this.labels);
  }

  async updateChannels() {
    if (!this.state.lshData) {
      return;
    }
    this.subplots = [];
    const values: number[][][] = await this.state.getWindow(this.topk);
    for (const idx in this.topk) {
      const window = values[idx];
      const data = [];
      this.state.selection.forEach((channelIndex, index) => {
        const channel = window[channelIndex];
        data.push({
          x: [...Array(channel.length).keys()],
          y: channel,
          type: 'line',
          xaxis: 'x',
          yaxis: `y${index + 2}`,
          line: {
            color: this.state.colors[channelIndex],
          }
        });
      });
      const subplots = [];
      this.state.selection.forEach((channelIndex, index) => {
        subplots.push([`xy${index + 2}`]);
      });
      const plot = {
        index: this.topk[idx],
        color: this.getColor(this.state.lshData.average_candidates.indexOf(this.topk[idx]) / this.state.lshData.average_candidates.length),
        data: data,
        layout: {
          grid: {
            rows: this.state.selection.length,
            columns: 1,
            subplots: subplots,
          },
          showlegend: false,
          hovermode: 'closest',
          autosize: true,
          margin: {
            l: 30,
            r: 30,
            t: 30,
            b: 0,
            pad: 4
          },
          height: 100 * this.state.selection.length,
          width: 300,
          titlefont: {
            size: 9
          },
          xaxis: {
            showgrid: false,
            zeroline: false,
            showticklabels: false,
          },
          yaxis: {
            zeroline: false,
            showticklabels: false,
          }
        }
      };
      this.state.selection.forEach((channelIndex, index) => {
        plot.layout[`yaxis${index + 2}`] = {
          zeroline: false,
          showticklabels: false,
        };
      });
      this.subplots.push(plot);
    }
  }

  async showSamples() {
    this.labels = [];
    this.topk = this.state.lshData.samples.filter((index) => this.state.labels[index] === undefined);

    this.subplots = [];
    const values: number[][][] = await this.state.getWindow(this.topk);
    for (const idx in this.topk) {
      const window = values[idx];
      const data = [];
      this.state.selection.forEach((channelIndex, index) => {
        const channel = window[channelIndex];
        data.push({
          x: [...Array(channel.length).keys()],
          y: channel,
          type: 'line',
          xaxis: 'x',
          yaxis: `y${index + 2}`,
          line: {
            color: this.state.colors[channelIndex],
          }
        });
      });
      const subplots = [];
      this.state.selection.forEach((channelIndex, index) => {
        subplots.push([`xy${index + 2}`]);
      });
      const plot = {
        index: this.topk[idx],
        color: this.getColor(this.state.lshData.average_candidates.indexOf(this.topk[idx]) / this.state.lshData.average_candidates.length),
        data: data,
        layout: {
          grid: {
            rows: this.state.selection.length,
            columns: 1,
            subplots: subplots,
          },
          showlegend: false,
          hovermode: 'closest',
          autosize: true,
          margin: {
            l: 30,
            r: 30,
            t: 30,
            b: 0,
            pad: 4
          },
          height: 100 * this.state.selection.length,
          width: 300,
          titlefont: {
            size: 9
          },
          xaxis: {
            showgrid: false,
            zeroline: false,
            showticklabels: false,
          },
          yaxis: {
            zeroline: false,
            showticklabels: false,
          }
        }
      };
      this.state.selection.forEach((channelIndex, index) => {
        plot.layout[`yaxis${index + 2}`] = {
          zeroline: false,
          showticklabels: false,
        };
      });
      this.subplots.push(plot);
    }
  }

  getColor(value) {
    const hue=((1-value)*120).toString(10);
    return ["hsl(",hue,",100%,50%)"].join("");
  }
}
