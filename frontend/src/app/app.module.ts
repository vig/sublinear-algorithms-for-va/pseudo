import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { OverviewWindowComponent } from './overview-window/overview-window.component';

import * as PlotlyJS from 'plotly.js/dist/plotly.js';
import { PlotlyModule } from 'angular-plotly.js';
import { FormsModule } from '@angular/forms';
import { LabelingWindowComponent } from './labeling-window/labeling-window.component';
import { TableOverviewComponent } from './table-overview/table-overview.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTabsModule } from '@angular/material/tabs';
import { LabelsComponent } from './labels/labels.component';
import { QueryWindowComponent } from './query-window/query-window.component';
import { ZingchartAngularModule } from 'zingchart-angular';
import { ProgressViewComponent } from './progress-view/progress-view.component';
import { MainComponent } from './main/main.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSliderModule } from '@angular/material/slider';
import { TrainingWindowComponent } from './training-window/training-window.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { StateManagementComponent } from './state-management/state-management.component';
import { AngularSplitModule } from 'angular-split';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

PlotlyModule.plotlyjs = PlotlyJS;

@NgModule({
  declarations: [
    AppComponent,
    OverviewWindowComponent,
    LabelingWindowComponent,
    TableOverviewComponent,
    LabelsComponent,
    QueryWindowComponent,
    ProgressViewComponent,
    MainComponent,
    TrainingWindowComponent,
    StateManagementComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    PlotlyModule,
    BrowserAnimationsModule,
    MatTabsModule,
    MatProgressBarModule,
    MatCheckboxModule,
    ZingchartAngularModule,
    MatSliderModule,
    AngularSplitModule,
    MatProgressSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
