import {Component, OnInit, ViewChild} from '@angular/core';
import {StateService} from '../state.service';
import * as d3 from 'd3';
import {TableInfoData} from '../api.service';

@Component({
  selector: 'app-progress-view',
  templateUrl: './progress-view.component.html',
  styleUrls: ['./progress-view.component.css']
})
export class ProgressViewComponent implements OnInit {
  @ViewChild('chart') chart;
  public plot;
  public data;
  public layout;
  public hist;
  public amountOfCandidates;
  public hover = 0;
  public prototypes;

  private _sliderValue = 0;

  constructor(private state: StateService) { }

  ngOnInit(): void {
    this.state.onNewLshData.subscribe(() => {
      this.showgraph();
      this.showHistogram();
    });
    this.state.onNewSelection.subscribe(() => { this.updateChannels(); });
  }

  showHistogram() {
    const table = this.state.lshData.average_table;
    this.hist = {
      data: [{
        x: Object.keys(table),
        y: Object.values(table).map((values: number[]) => values.length), // / (this.service.rawValues.length - this.service.windowSize)),
        type: 'bar',
        opacity: 0.5,
        marker: {
          color: Object.keys(table).map((key) => {
            return this.getColor(Number(key) / Number(Object.keys(table)[Object.keys(table).length - 1]));
          }),
          line: {
            color: 'black',
            width: 0,
          }
        }
      }],
      layout: {
        hovermode: 'closest',
        autosize: true,
        margin: {
          l: 0,
          r: 0,
          t: 0,
          b: 0,
          pad: 4
        },
        xaxis: {
          showticklabels: false
        },
        yaxis: {
          showticklabels: false
        },
        height: 200,
        width: 300,
      }
    };
  }

  onHover(data) {
    const x = data.points[0].x;
    const value = Object.keys(this.state.lshData.average_table).indexOf(x.toString());
    this.setSliderValue({value: Number(value)});
  }

  updateChannels() {
    if (!this.prototypes) {
      return;
    }
    const subplots = [];
    this.data = this.prototypes.map((prototype) => {
      const channelData = [];
      this.state.selection.forEach((channelIndex, index) => {
        const channel = prototype.max[channelIndex];
        channelData.push({
          x: [...Array(channel.length).keys()],
          y: channel,
          xaxis: 'x',
          yaxis: `y${index + 2}`,
          type: 'scatter',
          fill: null,
          mode: 'lines',
          line: {
            color: this.state.colors[channelIndex],
            width: 3
          }
        });
      });
      this.state.selection.forEach((channelIndex, index) => {
        const channel = prototype.min[channelIndex];
        channelData.push({
          x: [...Array(channel.length).keys()],
          y: channel,
          xaxis: 'x',
          yaxis: `y${index + 2}`,
          type: 'scatter',
          fill: 'tonexty',
          mode: 'lines',
          line: {
            color: this.state.colors[channelIndex],
            width: 3
          }
        });
      });
      this.state.selection.forEach((channelIndex, index) => {
        const channel = prototype.average[channelIndex];
        channelData.push({
          x: [...Array(channel.length).keys()],
          y: channel,
          xaxis: 'x',
          yaxis: `y${index + 2}`,
          type: 'line',
          line: {
            color: 'red',
            width: 3
          }
        });
      });
      return channelData;
    });
    for (let index = 0; index < this.state.selection.length; index++) {
      subplots.push([`xy${index + 2}`]);
    }
    this.layout = {
      grid: {
        rows: this.state.selection.length,
        columns: 1,
        subplots: subplots,
      },
      showlegend: false,
      hovermode: 'closest',
      autosize: true,
      margin: {
        l: 0,
        r: 0,
        t: 0,
        b: 0,
        pad: 4
      },
      xaxis: {
        showgrid: false,
        zeroline: false,
        showticklabels: false,
      },
      yaxis: {
        zeroline: false,
        showticklabels: false,
      },
      height: 200 * this.state.selection.length,
      width: 300,
    };
    this.state.selection.forEach((channelIndex, index) => {
      this.layout[`yaxis${index + 2}`] = {
        zeroline: false,
        showticklabels: false,
      };
    });
  }

  hoverPlot(averages) {
    const subplots = [];
    this.data = averages.map((prototype) => {
      const channelData = [];
      this.state.selection.forEach((channelIndex, index) => {
        const channel = prototype.max[channelIndex];
        channelData.push({
          x: [...Array(channel.length).keys()],
          y: channel,
          xaxis: 'x',
          yaxis: `y${index + 2}`,
          type: 'scatter',
          fill: null,
          mode: 'lines',
          line: {
            color: this.state.colors[channelIndex],
            width: 3
          }
        });
      });
      this.state.selection.forEach((channelIndex, index) => {
        const channel = prototype.min[channelIndex];
        channelData.push({
          x: [...Array(channel.length).keys()],
          y: channel,
          xaxis: 'x',
          yaxis: `y${index + 2}`,
          type: 'scatter',
          fill: 'tonexty',
          mode: 'lines',
          line: {
            color: this.state.colors[channelIndex],
            width: 3
          }
        });
      });
      this.state.selection.forEach((channelIndex, index) => {
        const channel = prototype.average[channelIndex];
        channelData.push({
          x: [...Array(channel.length).keys()],
          y: channel,
          xaxis: 'x',
          yaxis: `y${index + 2}`,
          type: 'line',
          line: {
            color: 'red',
            width: 3
          }
        });
      });
      return channelData;
    });
    for (let index = 0; index < this.state.selection.length; index++) {
      subplots.push([`xy${index + 2}`]);
    }
    this.layout = {
      grid: {
        rows: this.state.selection.length,
        columns: 1,
        subplots: subplots,
      },
      showlegend: false,
      hovermode: 'closest',
      autosize: true,
      margin: {
        l: 0,
        r: 0,
        t: 0,
        b: 0,
        pad: 4
      },
      xaxis: {
        showgrid: false,
        zeroline: false,
        showticklabels: false,
      },
      yaxis: {
        zeroline: false,
        showticklabels: false,
      },
      height: 100 * this.state.selection.length,
      width: 300,
    };
    this.state.selection.forEach((channelIndex, index) => {
      this.layout[`yaxis${index + 2}`] = {
        zeroline: false,
        showticklabels: false,
      };
    });
  }

  public setSliderValue(v) {
    this._sliderValue = v.value;
    const tableKey = Object.keys(this.state.lshData.average_table)[this._sliderValue];
    const data = this.hist;
    data.data[0].marker.line.width = Object.keys(this.state.lshData.average_table).map((key) => {
      return Number(key) === Number(tableKey) ? 4 : 0;
    });
    this.hist = data;
  }

  public get sliderValue(): number {
    return this._sliderValue;
  }

  public get nrOfCandidates(): number {
    let output = 0;
    for (let i = 0; i <= this.sliderValue; i++) {
      output += this.state.lshData.average_table[Object.keys(this.state.lshData.average_table)[i]].length;
    }
    return output;
  }

  public get maxLength(): number {
    return Object.keys(this.table).length - 1;
}

  public get table() {
    return this.state.lshData.average_table;
  }

  async showgraph() {
    const tableInfo: TableInfoData = await this.state.getTableInfo(Object.values(this.state.lshData.average_table));
    this.prototypes = tableInfo.prototypes;
    this.hoverPlot(tableInfo.prototypes);
  }

  getColor(value) {
    const hue=((1-value)*120).toString(10);
    return ["hsl(",hue,",100%,50%)"].join("");
  }

  public showCandidates() {
    this.state.sliderValue = this._sliderValue;
  }

  public get found(): string {
    return this.state.lshData.average_candidates.length.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }

  public get computingTime(): number {
    return this.state.computedTime / 1000;
  }
}
