import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { StateService } from '../state.service';
import zingchart from 'zingchart/es6';

@Component({
  selector: 'app-overview-window',
  templateUrl: './overview-window.component.html',
  styleUrls: ['./overview-window.component.css']
})
export class OverviewWindowComponent implements OnInit {
  @ViewChild('overview') chart;
  private initialHeight;
  public config;
  public graphset = [];
  public id = 'overview';
  public markers = [];
  public series = [];
  public goodLabels = [];
  public badLabels = [];
  public candidateLabels = [];
  public pathWidth = 100;
  public allChannels: number[];
  private _minx;
  private _maxx;
  public currentSelection;
  private showingCandidates = false;

  public data;
  public layout;

  constructor(private state: StateService, private el: ElementRef) {
    this.currentSelection = this.state.selection;
  }

  async ngOnInit(): Promise<void> {
    this.initialHeight = this.el.nativeElement.offsetHeight;
    this.state.onNewData.subscribe(() => this.initializePlot());
    this.state.onNewLshData.subscribe(() => {
      this.showingCandidates = false;
      this.updatePlot();
    });
    this.state.onNewSelection.subscribe(() => {
      this.currentSelection = this.state.selection;
      this.updateChannels();
      if (this.showingCandidates) {
        this.updateCandidates();
      }
    });
    this.state.onNewSlider.subscribe(() => {
      this.showingCandidates = true;
      this.updateCandidates();
    });
  }

  async initializePlot() {
    this.allChannels = [...Array(this.state.rawData.length).keys()];
    this.state.queryWindow = undefined;
    console.log(this.state.rawData[0].values.length - 1);
    console.log(this.state.rawData[0].index[this.state.rawData[0].index.length - 1]);
    // this.debugClicked();
    this.graphset.push({
      id: 'preview',
      type: "scatter",
      x: 0,
      y: 0,
      scaleX: {
        zooming: true,
        step: 1,
        minValue: 0,
        maxValue: this.state.rawData[0].values.length - 1,
        // zoomTo: [0, this.state.windowSize],
        // 'auto-fit': true,
        visible: false,
        guide: {
          visible: false
        }
      },
      height: '30px',
      scaleY: {
        maxValue: 1,
        minValue: -1,
        visible: false,
        guide: {
          visible: false
        }
      },
      preview: {
        x: 50,
        y: 10,
        height: '30px',
      },
      series: [
        {
          type: 'scatter',
          values: [],
          text: 'Good labels',
          marker: {
            backgroundColor: '#4caf50'
          },
          zIndex: 10,
        },
        {
          type: 'scatter',
          values: [],
          text: 'Bad labels',
          marker: {
            backgroundColor: '#f44336'
          },
          zIndex: 10,
        },
        {
          type: 'scatter',
          values: [],
          text: 'Candidates',
          marker: {
            backgroundColor: '#808080'
          },
          zIndex: 10,
        },
        {
          type: 'scatter',
          values: [[0, 1]],
          text: 'Bug',
          marker: {
            visible: false,
            backgroundColor: '#808080'
          },
          zIndex: 10,
        }
      ]
    });
    // Initialize channels

    this.state.selection.forEach((channelIndex, index) => {
      const channel = this.state.rawData[channelIndex];
      const data = [];
      // for (let i = 0; i < channel.values.length; i++) {
      //   data.push(channel.values[i]);
      // }
      console.log(data);
      this.graphset.push({
        id: index,
        x: 0,
        y: `${75 * index + 100}px`,
        type: 'line',
        height: '50px',
        scaleX: {
          zooming: true,
          'min-value': channel.index[0],
          // step: "6minute",
          // transform: {
          //   type: "date",
          //   all: "%m/%d/%Y<br>%h:%i"
          // },
          tick: {
            visible: index === 0,
          },
          item: {
            visible: index === 0,
          },
          placement: 'top',
        },
        'scale-y': {
          zooming: false,
          'auto-fit': true
        },
        plotarea: {
          height: '50px',
          'margin-top': '0px',
          'margin-bot': '0px'
        },
        series: [{
          values: channel.values,
          zIndex: 5,
          'line-color': this.state.colors[channelIndex],
          marker: {
            alpha: 0.0,
            zIndex: 10
          }
        }]
      });
    });
    zingchart.bind('overview', 'zoom', (e) => {
      console.log("zoom");
      if (e.stopupdate) {
        return;
      } else {
        this._minx = e.xmin;
        this._maxx = e.xmax;
      }
      if (e.graphid !== `overview-graph-preview`) {
        zingchart.exec(
          'overview',
          'zoomto', {
          graphid: `overview-graph-preview`,
          xmin: this._minx,
          xmax: this._maxx,
          stopupdate: true
        });
      }
      for (let i = 1; i < this.graphset.length; i ++) {
        if (i === e.graphid) {
          continue;
        }
        zingchart.exec(
          'overview',
          'zoomto', {
            graphid: i,
            xmin: this._minx,
            xmax: this._maxx,
            stopupdate: true
          });
      }
    });
    this.config = {
      layout: `${this.graphset.length}x1`,
      graphset: this.graphset
    };
    // this.chart.setdata({
    //   data: this.config
    // });
    console.log("showing plot");
    this._minx = 0;
    this._maxx = this.state.rawData[0].values.length / 1;
    zingchart.render({
      id: 'overview',
      data: this.config,
      output: 'canvas'
    });
  }


  async updateChannels() {
    console.log('updating plot');
    console.log(this.state.selection);
    this.graphset = this.graphset.slice(0, 1);
    this.state.selection.forEach((channelIndex, index) => {
      const channel = this.state.rawData[channelIndex];
      // const data = [];
      // for (let i = 0; i < channel.values.length; i++) {
      //   data.push([i, channel.values[i]]);
      // }
      this.graphset.push({
        id: channelIndex,
        x: 0,
        y: `${75 * index + 100}px`,
        type: 'line',
        height: '50px',
        scaleX: {
          zooming: true,
          'min-value': channel.index[0],
          // step: "6minute",
          // transform: {
          //   type: "date",
          //   all: "%m/%d/%Y<br>%h:%i"
          // },
          tick: {
            visible: index === 0,
          },
          item: {
            visible: index === 0,
          },
          placement: 'top',
        },
        'scale-y': {
          zooming: false,
          'auto-fit': true
        },
        plotarea: {
          height: '50px',
          'margin-top': '0px',
          'margin-bot': '0px'
        },
        series: [{
          type: 'line',
          values: channel.values,
          text: 'Raw Values',
          zIndex: 5,
          'line-color': this.state.colors[channelIndex],
          marker: {
            alpha: 0.0,
            zIndex: 10
          }
        }]
      });
    });
    this.config = {
      layout: `${this.graphset.length}x1`,
      graphset: this.graphset
    };
    console.log("showing plot");
    zingchart.exec(
      'overview',
      'setdata',
      { data: this.config }
    );
  }

  async updatePlot() {
    console.log('updating plot');
    if (!this.state.queryWindow) {
      return;
    }
    this.goodLabels = [];
    this.badLabels = [];
    this.markers = [];
    for (const index in this.state.labels) {
      if (this.state.labels[index]) {
        this.goodLabels.push([Number(index), 1]);
        this.markers.push({
          type: 'area',
          // BUG: For some reason the range values are multiplied by 1
          range: [Number(index) / 1, (Number(index) + this.state.windowSize) / 1],
          backgroundColor: "#4caf50",
        });
      } else {
        this.badLabels.push([Number(index), -1]);
        this.markers.push({
          type: 'area',
          // BUG: For some reason the range values are multiplied by 1
          range: [Number(index) / 1, (Number(index) + this.state.windowSize) / 1],
          backgroundColor: "#f44336",
        });
      }
    }
    for (const index of this.state.lshData.samples) {
      this.candidateLabels.push([Number(index), 0]);
      this.markers.push({
        type: 'area',
        // BUG: For some reason the range values are multiplied by 1
        range: [Number(index) / 1, (Number(index) + this.state.windowSize) / 1],
        backgroundColor: '#808080',
      });
    }
    console.log(this.markers);
    for (const channel of this.config.graphset) {
      if (channel.type === 'line') {
        channel.scaleX.markers = this.markers;
      } else {
        channel.series[0].values = this.goodLabels;
        channel.series[1].values = this.badLabels;
        channel.series[2].values = this.candidateLabels;
        channel.series[2].marker.backgroundColor = '#808080';
      }
    }
    zingchart.exec(
      'overview',
      'setdata',
      { data: this.config }
    );
  }

  ping() {
    console.log("ping");
  }

  async updateCandidates() {
    let candidates = [];
    for (let i = 0; i <= this.state.sliderValue; i++) {
      candidates = candidates.concat(this.state.lshData.average_table[Object.keys(this.state.lshData.average_table)[i]]);
    }
    console.log('updating candidates');
    console.log(candidates);

    this.candidateLabels = [];
    this.goodLabels = [];
    this.badLabels = [];
    this.markers = [];

    for (const index in this.state.labels) {
      if (this.state.labels[index]) {
        this.goodLabels.push([Number(index), 1]);
        this.markers.push({
          type: 'area',
          // BUG: For some reason the range values are multiplied by 1
          range: [Number(index) / 1, (Number(index) + this.state.windowSize) / 1],
          backgroundColor: "#4caf50",
        });
      } else {
        this.badLabels.push([Number(index), -1]);
        this.markers.push({
          type: 'area',
          // BUG: For some reason the range values are multiplied by 1
          range: [Number(index) / 1, (Number(index) + this.state.windowSize) / 1],
          backgroundColor: "#f44336",
        });
      }
    }
    for (const index of candidates) {
      this.candidateLabels.push([Number(index), 0]);
      this.markers.push({
        type: 'area',
        // BUG: For some reason the range values are multiplied by 1
        range: [Number(index) / 1, (Number(index) + this.state.windowSize) / 1],
        backgroundColor: '#b1a343',
      });
    }

    for (const channel of this.config.graphset) {
      if (channel.type === 'line') {
        channel.scaleX.markers = this.markers;
      } else {
        channel.series[0].values = this.goodLabels;
        channel.series[1].values = this.badLabels;
        channel.series[2].values = this.candidateLabels;
        channel.series[2].marker.backgroundColor = '#b1a343';
      }
    }
    zingchart.exec(
      'overview',
      'setdata',
      { data: this.config }
    );
  }

  scroll(p) {
    p.stopPropagation();
    console.log('mousewheel');
    console.log(p);
    console.log(p.deltaY);
    const q = Math.round((this._maxx - this._minx) / 1);
    if (p.deltaY > 0) {
      this._minx = Math.max(this._minx - q, 0);
      this._maxx = Math.max(this._maxx - q, 0);
    } else if (p.deltaY < 0) {
      this._minx = Math.min(this._minx + q, this.state.rawData[0].values.length - 1);
      this._maxx = Math.min(this._maxx + q, this.state.rawData[0].values.length - 1);
    }
    zingchart.exec(
      'overview',
      'zoomto', {
        graphid: `overview-graph-preview`,
        xmin: this._minx,
        xmax: this._maxx,
      });
  }

  isInSelection(i): boolean {
    return this.currentSelection.includes(i);
  }

  getColor(i): string {
    return this.state.colors[i];
  }

  changeSelection(i: number): void {
    if (this.isInSelection(i)) {
      this.currentSelection = this.currentSelection.filter((v) => v !== i);
    } else {
      this.currentSelection.push(i);
    }
  }

  setSelection(): void {
    this.state.selection = this.currentSelection;
  }

  get height(): number {
    return Math.max(75 * this.state.selection.length + 50, this.initialHeight);
  }

  async setquery() {
    this.state.windowSize = this.maxx - this.minx;
    await this.state.getQueryWindow(null, this.maxx - this.minx, this.minx);
    await this.state.lshInitial();
  }

  get maxx(): number {
    return this._maxx * 1;
  }

  set maxx(max: number) {
    this._maxx = Math.min(max / 1, this.state.rawData[0].values.length - 1);
    zingchart.exec(
      'overview',
      'zoomto', {
        graphid: `overview-graph-preview`,
        xmin: this._minx,
        xmax: this._maxx,
        stopupdate: true
      });
    for (let i = 1; i < this.graphset.length; i ++) {
      zingchart.exec(
        'overview',
        'zoomto', {
          graphid: i,
          xmin: this._minx,
          xmax: this._maxx,
          stopupdate: true
        });
    }
  }

  get minx(): number {
    return this._minx * 1;
  }

  set minx(min: number) {
    this._minx = Math.max(min / 1, 0);
    zingchart.exec(
      'overview',
      'zoomto', {
        graphid: `overview-graph-preview`,
        xmin: this._minx,
        xmax: this._maxx,
        stopupdate: true
      });
    for (let i = 1; i < this.graphset.length; i ++) {
      zingchart.exec(
        'overview',
        'zoomto', {
          graphid: i,
          xmin: this._minx,
          xmax: this._maxx,
          stopupdate: true
        });
    }
  }

  get windowsize(): number {
    return this.maxx - this.minx;
  }

  set windowsize(size: number) {
    this.maxx = this.minx + size;
  }

  get names(): string[] {
    return this.state.rawData.map((data, index) => data.name ? data.name : 'Channel ' + index.toString());
  }
}
