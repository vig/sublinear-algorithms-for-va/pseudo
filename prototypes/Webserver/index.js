const PLOT = document.getElementById('main-plot');
const SUBPLOTS = document.getElementById('subplots');
const FORM = document.getElementById('parameters');

var default_colors = [];
var default_sizes = [];
var default_opacity = [];
var indices, values, windows, tables, hash_functions;
var plot;
var selectedIndex;

// Start the application
async function runApp() {
    await readFile();
    await createWindows();
    await createTables();
    makePlot();
}

// Generate new tables
async function updateTables() {
    await createTables();
    let click_data = {
        points: [
            {
                pointNumber: this.selectedIndex
            }
        ]
    };
    await updatePlot(click_data);
}

// Reset everything (parameters, windows) except for input data
async function reset() {
    await createWindows();
    await createTables();
    SUBPLOTS.innerHTML = '';
    var update = {'marker':{color: this.default_colors.slice(), size:this.default_sizes.slice()}};
    Plotly.restyle(PLOT, update);
}

// Read input data
async function readFile() {
    const response = await fetch('http://127.0.0.1:5000/read-data');
    const data = await response.json(); //extract JSON from the http response
    this.indices = data.index;
    this.values = data.values;
}

// Split data into windows and normalize
async function createWindows() {
    let postData = {values: this.values}
    const parameters = new FormData(FORM);
    for (let [key, value] of parameters.entries()) {
        postData[key] = value
    }
    const response = await fetch('http://127.0.0.1:5000/create-windows', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(postData)
    });
    this.windows = await response.json();
}

// Generate LSH-tables by hashing each window
async function createTables() {
    let postData = {windows: this.windows}
    const parameters = new FormData(FORM);
    for (let [key, value] of parameters.entries()) {
        postData[key] = value
    }
    const response = await fetch('http://127.0.0.1:5000/create-tables', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(postData)
    });
    const data = await response.json();
    this.tables = data.tables;
    this.hash_functions = data.hash_functions;
}

// Return similar windows based on an input query
async function getSimilarWindows(window) {
    const response = await fetch('http://127.0.0.1:5000/query', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({window: window, tables: this.tables, hash_functions: this.hash_functions})
    });
    const data = await response.json();
    console.log(data);
    return data;
}

// Draw the main plot that shows all the data
function makePlot() {
    for (var i = 0; i < this.values.length; i++) {
        this.default_colors.push('#a3a7e4');
        this.default_sizes.push(5);
        this.default_opacity.push(1);
    }
    this.plot = [{
        x: this.indices,
        y: this.values,
        type:'scatter',
        mode:'markers', marker:{size:this.default_sizes.slice(), color: this.default_colors.slice()}
    }]
    var layout = {
        hovermode:'closest',
        autosize: true,
        margin: {
            l: 0,
            r: 0,
            b: 40,
            t: 0,
            pad: 4
        },
        height: 200
    };
    Plotly.newPlot(PLOT, plot, layout);

    PLOT.on('plotly_click', function(click_data){
        updatePlot(click_data)
    })
}

// Update the plot depending on the selected point
async function updatePlot(click_data) {
    let table_size = Number(document.getElementById("tablesize").value);
    var pn='',
        colors=this.default_colors.slice(),
        sizes=this.default_sizes.slice(),
        opacity=this.default_opacity.slice();

    for(var i=0; i < click_data.points.length; i++){
        pn = click_data.points[i].pointNumber;
    }
    if (pn === undefined) {
        return;
    }
    this.selectedIndex = pn;
    console.log(this.selectedIndex);
    let similar_indices = await this.getSimilarWindows(this.windows[pn]);
    for (let frequency in similar_indices){
        for (let i = 0; i < similar_indices[frequency].length; i++) {
            colors[similar_indices[frequency][i]] = getColor(frequency/table_size);
            sizes[similar_indices[frequency][i]] = (frequency/table_size)*10;
            opacity[similar_indices[frequency][i]] = frequency/table_size;
        }
    }

    //createSubPlot(pn);
    //let nr_of_subplots = Math.min(similar_indices[table_size].length, 4);
    let k = 5
    let topKWindows = getTopKSimilar(similar_indices, k);
    createSubPlots(topKWindows);

    var update = {'marker':{color: colors, size:sizes, opacity: opacity}};
    Plotly.restyle(PLOT, update);
}

// Create subplots for each similar window
function createSubPlots(indices) {
    SUBPLOTS.innerHTML = '';
    let windowsize = Number(document.getElementById("windowsize").value);
    indices.forEach(similar => {
        let newDiv = document.createElement("div");
        let plotDiv = document.createElement("div");
        let button = document.createElement("button");
        button.innerHTML = 'Label correct';
        button.onclick = async function () {
            console.log(selectedIndex);
            await updateTables();
        }
        newDiv.appendChild(plotDiv);
        newDiv.appendChild(button);
        SUBPLOTS.appendChild(newDiv);
        let subplot= [{
            x: this.indices.slice(similar.index, similar.index + windowsize),
            y: this.values.slice(similar.index, similar.index + windowsize),
            type:'line'
        }]
        let layout = {
            title: `Index: ${similar.index.toString()}      Similarity: ${similar.frequency.toString()}%`,
            hovermode:'closest',
            autosize: true,
            margin: {
                l: 30,
                r: 30,
                t: 30,
                pad: 4
            },
            height: 200,
            width: 200,
            titlefont: {
                size: 9
            },
        };
        Plotly.newPlot(newDiv, subplot, layout);
    })
}

function getColor(value) {
    //value from 0 to 1
    let hue_value = Math.min((1-value) * 2, 1)
    let hue = ((1 - hue_value) * 120).toString(10);
    let sat = value * 100;
    return `hsl(${hue}, ${sat}%, 50%)`;
}

function getTopKSimilar(windows_by_frequency, k) {
    let topk = [];
    let tablesize = Number(document.getElementById("tablesize").value);
    const keys = Object.keys(windows_by_frequency).map(a => Number(a)).sort(function(a, b){return b-a});
    for (let i = 0; i < keys.length; i++) {
        let windows = windows_by_frequency[keys[i]];
        let nrOfWindows = windows.length;
        if (nrOfWindows >= k) {
            let temp = windows.slice(0,k).map(index => {return {index: index, frequency: 100 * keys[i]/tablesize}})
            topk = topk.concat(temp);
            break;
        } else {
            k -= nrOfWindows;
            let temp = windows.map(index => {return {index: index, frequency: 100 * keys[i]/tablesize}})
            topk = topk.concat(temp);
        }
    }
    return topk;
}

runApp()
