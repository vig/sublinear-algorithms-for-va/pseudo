install:
	conda env create -f ./environment.yml

server:
	cd backend && export FLASK_APP=src/main.py && export FLASK_ENV=development && export FLASK_DEBUG=1 && flask run

ui:
	cd frontend && ng serve --open
